<?php

namespace ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HappyHourTimeTableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weekday', ChoiceType::class, array(
                'label'   => 'Dia de la semana',
                'choices' => array(
                    1 => 'Lunes',
                    2 => 'Martes',
                    3 => 'Miercoles',
                    4 => 'Jueves',
                    5 => 'Viernes',
                    6 => 'Sabado',
                    7 => 'Domingo',
                ),
            ))
            ->add('starts_at', null, array(
                    'label' => 'Hora inicio',
                    'input' => 'datetime'
                )
            )
            ->add('ends_at', null, array(
                    'label' => 'Hora termino',
                    'input' => 'datetime'
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\HappyHourTimeTable',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'clientbundle_hh';
    }
}
