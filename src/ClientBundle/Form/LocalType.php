<?php

namespace ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LocalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'label' => 'Nombre',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('status_text', null, array(
                'label' => 'Estado',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('file', null, array(
                'label' => 'Imagen',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('enabled', null, array(
                'label' => 'Local',
                'attr'  => array(
                    'data-on-text'  => 'Activado',
                    'data-off-text' => 'Desactivado',
                    'class'         => 'make-switch'
                )
            ))
            ->add('latitude', null, array(
                'label' => 'Latitude',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('longitude', null, array(
                'label' => 'Longitude',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('order_timeout', null, array(
                'label' => 'Tiempo de espera de la orden',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('credit_timeout', null, array(
                'label' => 'Tiempo de espera de crédito',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('happy_hour_enabled', null, array(
                'label' => 'Happy Hour',
                'attr'  => array(
                    'data-on-text'  => 'Activado',
                    'data-off-text' => 'Desactivado',
                    'class'         => 'make-switch'
                )
            ))
            ->add('happy_hour_timetable', null, array(
                'label' => 'Comienzo del Happy Hour',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('event_type', null, array(
                'label' => 'Tipo de Evento',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Local',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_admin';
    }
}
