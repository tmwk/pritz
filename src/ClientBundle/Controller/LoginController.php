<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 18:13
 */

namespace ClientBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class LoginController
 * @package ClientBundle\Controller
 * @Route("/")
 */
class LoginController extends Controller
{
    /**
     * @Route("/login", name="client_login_route")
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('ClientBundle:login:index.html.twig', array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );

    }

    /**
     * @Route("/login_check", name="client_login_check")
     */
    public function loginCheckAction()
    {
    }

    /**
     * @Route("/logout", name="client_logout")
     */
    public function logoutAction()
    {
        return array();
    }
}