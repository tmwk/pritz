<?php

namespace ClientBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package ClientBundle\Controller
 * @Route("/")
 */
class DefaultController extends FOSRestController
{
    /**
     * @return mixed
     * @Route("/", name="client_home")
     */
    public function indexAction()
    {
        return $this->render('ClientBundle:default:index.html.twig', array(
            'context' => 'Client'
        ));
    }
}
