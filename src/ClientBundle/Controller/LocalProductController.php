<?php

namespace ClientBundle\Controller;

use ApiBundle\Entity\Client;
use ApiBundle\Entity\Local;
use ApiBundle\Entity\Product;
use ClientBundle\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LocalProductController
 * @package ClientBundle\Controller
 * @Route("/local/{local}/product")
 */
class LocalProductController extends Controller
{
    /**
     * @Route("/", name="client_local_product")
     * @param Local $local
     * @return Response
     */
    public function indexAction(Local $local)
    {
        return $this->render('ClientBundle:local_product:index.html.twig', array(
            'local'  => $local,
        ));
    }

    /**
     * @Route("/create", name="client_local_product_create")
     * @param Request $request
     * @param Local $local
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request, Local $local)
    {
        $em     = $this->getDoctrine()->getManager();
        $entity = new Product();
        $form   = $this->createForm(new ProductType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity->setLocal($local);
            $entity->setCreateDate(new \DateTime('now'));
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('client_local_product', array('local' => $local->getId()));
        }

        $var = $this->renderView('ClientBundle:local_product:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'local'  => $local
        ));

        return new Response($var, 404);
    }

    /**
     * @Route("/new", name="client_local_product_new")
     * @param Local $local
     * @return Response
     */
    public function newAction(Local $local)
    {
        $form = $this->createForm(new ProductType(), new Product());

        return $this->render('ClientBundle:local_product:new.html.twig', array(
            'form'   => $form->createView(),
            'local'  => $local,
        ));
    }

    /**
     * @Route("/{product}/edit", name="client_local_product_edit")
     * @param Local $local
     * @param Product $product
     * @return Response
     */
    public function editAction(Local $local, Product $product)
    {
        if (!$product) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $editForm = $this->createForm(new ProductType(), $product);

        return $this->render('ClientBundle:local_product:edit.html.twig', array(
            'local'     => $local,
            'product'   => $product,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * @Route("/{product}/update", name="client_local_product_update")
     * @param Request $request
     * @param Local $local
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateAction(Request $request, Local $local, Product $product)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$product) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $editForm = $this->createForm(new ProductType(), $product);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $local->setLastUpdate(new \DateTime('now'));
            $em->flush();

            return $this->redirectToRoute('client_local_product_edit', array('local' => $local->getId(), 'product' => $product->getId()));
        }

        $var = $this->renderView('ClientBundle:local:edit.html.twig', array(
            'local'     => $local,
            'product'   => $product,
            'edit_form' => $editForm->createView(),
        ));

        return new Response($var, 404);
    }

    /**
     * @Route("/{product}/delete", name="client_local_product_delete")
     * @param Local $local
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Local $local, Product $product)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$product) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('client_local_product', array('local' => $local->getId()));
    }

    /**
     * @Route("/json", name="client_local_product_json")
     * @param Local $local
     * @return JsonResponse
     */
    public function jsonAction(Local $local)
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ApiBundle:Product')->findBy(array('local' => $local->getId()));

        $array = array();
        foreach ($entities as $val) {

            $options = '';

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('client_local_product_edit', array('local' => $local->getId(), 'product' => $val->getId())),
                'class' => 'iframe',
                'icon'  => 'icon-pencil',
                'text'  => 'Editar'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('client_local_product_delete', array('local' => $local->getId(), 'product' => $val->getId())),
                'class'       => 'del-ajax',
                'icon'        => 'icon-trash',
                'text'        => 'Eliminar',
                'data_reload' => 'reload_client_local_product'
            ));

            $array[] = array(
                $val->getNumbers(),
                $val->getName(),
                $val->getPrice(),
                $this->renderView(':Templates:dropdown.html.twig', array(
                    'options' => $options
                ))
            );
        }

        $response = new JsonResponse();
        $response->setContent(json_encode(array('aaData' => $array)));
        return $response;
    }
}
