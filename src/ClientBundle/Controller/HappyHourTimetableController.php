<?php

namespace ClientBundle\Controller;

use ApiBundle\Entity\HappyHourTimetable;
use ApiBundle\Entity\Local;
use ClientBundle\Form\HappyHourTimeTableType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LocalBartenderController
 * @package ClientBundle\Controller
 * @Route("/local/{local}/happy_hour")
 */
class HappyHourTimetableController extends Controller
{
    /**
     * @Route("/", name="client_local_happy_hour")
     * @param Local $local
     * @return Response
     */
    public function indexAction(Local $local)
    {
        return $this->render('ClientBundle:happy_hour:index.html.twig', array(
            'local' => $local,
        ));
    }

    /**
     * @Route("/create", name="client_local_happy_hour_create")
     * @param Request $request
     * @param Local $local
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request, Local $local)
    {
        $em     = $this->getDoctrine()->getManager();
        $entity = new HappyHourTimetable();
        $form   = $this->createForm(new HappyHourTimeTableType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity->setCreateDate(new \DateTime());
            $entity->setLocal($local);
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('client_local_happy_hour', array('local' => $local->getId()));
        }

        $var = $this->renderView('ClientBundle:happy_hour:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'local'  => $local
        ));

        return new Response($var, 404);
    }

    /**
     * @Route("/new", name="client_local_happy_hour_new")
     * @param Local $local
     * @return Response
     */
    public function newAction(Local $local)
    {
        $entity = new HappyHourTimetable();
        $form   = $this->createForm(new HappyHourTimeTableType(), $entity);

        return $this->render('ClientBundle:happy_hour:new.html.twig', array(
            'form'  => $form->createView(),
            'local' => $local,
        ));
    }

    /**
     * @Route("/{happy_hour}/edit", name="client_local_happy_hour_edit")
     * @param Local $local
     * @param HappyHourTimetable $happy_hour
     * @return Response
     */
    public function editAction(Local $local, HappyHourTimetable $happy_hour)
    {
        if (!$happy_hour) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $editForm = $this->createForm(new HappyHourTimeTableType(), $happy_hour);

        return $this->render('ClientBundle:happy_hour:edit.html.twig', array(
            'local'      => $local,
            'happy_hour' => $happy_hour,
            'edit_form'  => $editForm->createView(),
        ));
    }

    /**
     * @Route("/{happy_hour}/update", name="client_local_happy_hour_update")
     * @param Request $request
     * @param Local $local
     * @param HappyHourTimetable $happy_hour
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateAction(Request $request, Local $local, HappyHourTimetable $happy_hour)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$happy_hour) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $editForm = $this->createForm(new HappyHourTimeTableType(), $happy_hour);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $local->setLastUpdate(new \DateTime('now'));
            $em->flush();

            return $this->redirectToRoute('client_local_happy_hour_edit', array('local' => $local->getId(), 'happy_hour' => $happy_hour->getId()));
        }

        $var = $this->renderView('ClientBundle:happy_hour:edit.html.twig', array(
            'local'     => $local,
            'product'   => $happy_hour,
            'edit_form' => $editForm->createView(),
        ));

        return new Response($var, 404);
    }

    /**
     * @Route("/{happy_hour}/delete", name="client_local_happy_hour_delete")
     * @param Local $local
     * @param HappyHourTimetable $happy_hour
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Local $local, HappyHourTimetable $happy_hour)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$happy_hour) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $em->remove($happy_hour);
        $em->flush();

        return $this->redirectToRoute('client_local_happy_hour', array('local' => $local->getId()));
    }


    /**
     * @Route("/json", name="client_local_happy_hour_json")
     * @param Local $local
     * @return JsonResponse
     */
    public function jsonAction(Local $local)
    {
        $entities = $this->getDoctrine()->getRepository('ApiBundle:HappyHourTimetable')->findBy(array('local' => $local->getId()));

        $array = array();
        foreach ($entities as $val) {

            $options = '';

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('client_local_happy_hour_edit', array('local' => $local->getId(), 'happy_hour' => $val->getId())),
                'class'       => 'iframe',
                'icon'        => 'icon-pencil',
                'text'        => 'Editar',
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('client_local_happy_hour_delete', array('local' => $local->getId(), 'happy_hour' => $val->getId())),
                'class'       => 'del-ajax',
                'icon'        => 'icon-trash',
                'text'        => 'Eliminar',
                'data_reload' => 'reload_client_local_happy_hour'
            ));

            $array[] = array(
                $val->getNumbers(),
                $val->getWeekdayString(),
                date_format($val->getStartsAt(), 'H:i'),
                date_format($val->getEndsAt(), 'H:i'),
                $this->renderView(':Templates:dropdown.html.twig', array(
                    'options' => $options
                ))
            );
        }

        $response = new JsonResponse();
        $response->setContent(json_encode(array('aaData' => $array)));
        return $response;
    }
}
