<?php

namespace ClientBundle\Controller;

use ApiBundle\Entity\Local;
use ApiBundle\Entity\ProductCategory;
use ClientBundle\Form\LocalType;
use ClientBundle\Form\ProductCategoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Local controller.
 *
 * @Route("/local")
 */
class LocalController extends Controller
{

    /**
     * Lists all Local entities.
     *
     * @Route("/", name="client_local")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('ClientBundle:local:index.html.twig');
    }

    /**
     * Creates a new Local entity.
     *
     * @Route("/create", name="client_local_create")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $entity = new Local();
        $form   = $this->createForm(new LocalType(), $entity);
        $user = $this->getUser();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em     = $this->getDoctrine()->getManager();
            $client = $em->getRepository('ApiBundle:Client')->find($user->getId());

            $entity->setCreateDate(new \DateTime('now'));
            $entity->setClient($client);
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('client_local');
        }

        $var = $this->renderView('ClientBundle:local:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));

        return new Response($var, 404);
    }


    /**
     * Displays a form to create a new Local entity.
     *
     * @Route("/new", name="client_local_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $entity = new Local();
        $form   = $this->createForm(new LocalType(), $entity);

        return $this->render('ClientBundle:local:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Local entity.
     *
     * @Route("/{local}/edit", name="client_local_edit")
     * @Method("GET")
     * @param Local $local
     * @return Response
     */
    public function editAction(Local $local)
    {
        if (!$local) {
            throw $this->createNotFoundException('Unable to find Local entity.');
        }

        $editForm = $this->createForm(new LocalType(), $local);

        return $this->render('ClientBundle:local:edit.html.twig', array(
            'entity'    => $local,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Local entity.
     *
     * @Route("/{local}/update", name="client_local_update")
     * @Method("POST")
     * @param Request $request
     * @param Local $local
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateAction(Request $request, Local $local)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$local) {
            throw $this->createNotFoundException('Unable to find Local entity.');
        }

        $editForm = $this->createForm(new LocalType(), $local);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $local->setLastUpdate(new \DateTime());
            $em->flush();

            return $this->redirect($this->generateUrl('client_local_edit', array('local' => $local->getId())));
        }

        $var = $this->renderView('AdminBundle:local:edit.html.twig', array(
            'entity'    => $local,
            'edit_form' => $editForm->createView(),
        ));

        return new Response($var, 404);
    }

    /**
     * Deletes a Local entity.
     *
     * @Route("/{local}/delete", name="client_local_delete")
     * @param Local $local
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Local $local)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$local) {
            throw $this->createNotFoundException('Unable to find Local entity.');
        }

        $em->remove($local);
        $em->flush();

        return $this->redirectToRoute('client_local');
    }

    /**
     * @Route("/json", name="client_local_json")
     */
    public function jsonAction()
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ApiBundle:Client')->find($this->getUser()->getId());
        $array    = array();

        foreach ($entities->getLocal() as $val) {
            $options = '';

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('client_local_bartender', array('local' => $val->getId())),
                'icon'  => 'icon-user',
                'text'  => 'Barman'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('client_local_product', array('local' => $val->getId())),
                'icon'  => 'icon-social-dropbox',
                'text'  => 'Productos'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('client_local_happy_hour', array('local' => $val->getId())),
                'icon'  => 'icon-clock',
                'text'  => 'Horarios Happy Hours'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('client_local_edit', array('local' => $val->getId())),
                'class' => 'iframe',
                'icon'  => 'icon-pencil',
                'text'  => 'Editar'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('client_local_delete', array('local' => $val->getId())),
                'class'       => 'del-ajax',
                'icon'        => 'icon-trash',
                'text'        => 'Eliminar',
                'data_reload' => 'reload_client_local'
            ));
            $array[] = array(
                $val->getNumbers(),
                $val->getName(),
                $val->getEnabled() ? '<span class="label label-success label-sm">Activado</span>' : '<span class="label label-danger label-sm">Desactivado</span>',
                $this->renderView(':Templates:dropdown.html.twig', array(
                    'options' => $options
                ))
            );
        }

        $response = new Response();
        $response->setContent(json_encode(array(
            'aaData' => $array,
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
