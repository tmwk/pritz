<?php

namespace ClientBundle\Controller;

use ApiBundle\Entity\ProductCategory;
use ClientBundle\Form\ProductCategoryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AdminBundle\Entity\Admin;
use AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

/**
 * ProductCategory controller.
 *
 * @Route("/categorias")
 */
class ProductCategoryController extends Controller
{

    /**
     * Lists all ProductCategory entities.
     *
     * @Route("/", name="client_product_category")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ApiBundle:ProductCategory')->findAll();

        return $this->render('ClientBundle:product_category:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Admin entity.
     *
     * @Route("/create", name="client_product_category_create")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $entity = new ProductCategory();
        $form   = $this->createForm(new ProductCategoryType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setCreateDate(new \DateTime('now'));
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('client_product_category');
        }

        $var = $this->renderView('ClientBundle:product_category:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));

        return new Response($var, 404);
    }


    /**
     * Displays a form to create a new Admin entity.
     *
     * @Route("/new", name="client_product_category_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $entity = new ProductCategory();
        $form   = $this->createForm(new ProductCategoryType(), $entity);

        return $this->render('ClientBundle:product_category:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Admin entity.
     *
     * @Route("/{category}/edit", name="client_product_category_edit")
     * @Method("GET")
     * @param ProductCategory $category
     * @return Response
     */
    public function editAction(ProductCategory $category)
    {
        if (!$category) {
            throw $this->createNotFoundException('Unable to find Admin entity.');
        }

        $editForm = $this->createForm(new ProductCategoryType(), $category);

        return $this->render('ClientBundle:product_category:edit.html.twig', array(
            'entity'    => $category,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Admin entity.
     *
     * @Route("/{category}/update", name="client_product_category_update")
     * @Method("POST")
     * @param Request $request
     * @param ProductCategory $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateAction(Request $request, ProductCategory $category)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$category) {
            throw $this->createNotFoundException('Unable to find Admin entity.');
        }

        $editForm = $this->createForm(new ProductCategoryType(), $category);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('client_product_category_edit', array('category' => $category->getId())));
        }

        $var = $this->renderView('AdminBundle:admin:edit.html.twig', array(
            'entity'    => $category,
            'edit_form' => $editForm->createView(),
        ));

        return new Response($var, 404);
    }

    /**
     * Deletes a Admin entity.
     *
     * @Route("/{category}/delete", name="client_product_category_delete")
     * @param ProductCategory $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(ProductCategory $category)
    {
        $em     = $this->getDoctrine()->getManager();

        if (!$category) {
            throw $this->createNotFoundException('Unable to find Admin entity.');
        }

        $em->remove($category);
        $em->flush();

        return $this->redirect($this->generateUrl('client_product_category'));
    }

    /**
     * @Route("/json", name="client_product_category_json")
     */
    public function jsonAction()
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ApiBundle:ProductCategory')->findAll();
        $array    = array();
        foreach ($entities as $val) {
            $options = '';
            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('client_product_category_edit', array('category' => $val->getId())),
                'class' => 'iframe',
                'icon'  => 'icon-pencil',
                'text'  => 'Editar'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('client_product_category_delete', array('category' => $val->getId())),
                'class'       => 'del-ajax',
                'icon'        => 'icon-trash',
                'text'        => 'Eliminar',
                'data_reload' => 'reload_client_product_category'
            ));
            $array[] = array(
                $val->getNumbers(),
                $val->getName(),
                $this->renderView(':Templates:dropdown.html.twig', array(
                    'options' => $options
                ))
            );
        }

        $response = new Response();
        $response->setContent(json_encode(array(
            'aaData' => $array,
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
