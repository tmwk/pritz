<?php

namespace ClientBundle\Controller;

use ClientBundle\Form\BarmanType;
use ApiBundle\Entity\Client;
use ApiBundle\Entity\Local;
use ApiBundle\Entity\QrScanPermission;
use ApiBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LocalBartenderController
 * @package ClientBundle\Controller
 * @Route("/local/{local}/bartender")
 */
class LocalBartenderController extends Controller
{
    /**
     * @Route("/", name="client_local_bartender")
     * @param Local $local
     * @return Response
     */
    public function indexAction(Local $local)
    {
        return $this->render('ClientBundle:local_bartender:index.html.twig', array(
            'local'  => $local,
        ));
    }

    /**
     * @Route("/create", name="client_local_bartender_create")
     * @param Request $request
     * @param Local $local
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request, Local $local)
    {
        $em     = $this->getDoctrine()->getManager();
        $help   = $this->get('util.helper');
        $barman = new User();
        $qr     = new QrScanPermission();
        $form   = $this->createForm(new BarmanType(), $barman);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $pass = $help->randomPass(8);

            $qr->setLocal($local);
            $qr->setUser($barman);
            $qr->setCreateDate(new \DateTime('now'));
            $barman->setPassword($pass);
            $barman->setRegisterDate(new \DateTime('now'));

            $em->persist($barman);
            $em->persist($qr);
            $em->flush();

            $this->sendPass($barman, $pass);

            return $this->redirectToRoute('client_local_bartender', array('local' => $local->getId()));
        }

        $var = $this->renderView('ClientBundle:local_bartender:new.html.twig', array(
            'entity' => $barman,
            'form'   => $form->createView(),
            'local'  => $local
        ));

        return new Response($var, 404);
    }

    /**
     * @Route("/new", name="client_local_bartender_new")
     * @param Local $local
     * @return Response
     */
    public function newAction(Local $local)
    {
        $form = $this->createForm(new BarmanType(), new User());

        return $this->render('ClientBundle:local_bartender:new.html.twig', array(
            'form'   => $form->createView(),
            'local'  => $local,
        ));
    }

    /**
     * @Route("/{barman}/delete", name="client_local_bartender_delete")
     * @param Local $local
     * @param QrScanPermission $barman
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Local $local, QrScanPermission $barman)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$barman) {
            throw $this->createNotFoundException('Unable to find Barman entity.');
        }

        $em->remove($barman->getUser());
        $em->remove($barman);
        $em->flush();

        return $this->redirectToRoute('client_local_bartender', array('local' => $local->getId()));
    }


    /**
     * @Route("/json", name="client_local_bartender_json")
     * @param Local $local
     * @return JsonResponse
     */
    public function jsonAction(Local $local)
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ApiBundle:QrScanPermission')->findBy(array('local' => $local->getId()));

        $array = array();
        foreach ($entities as $val) {

            $options = '';

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'           => $this->generateUrl('client_local_bartender_send_pass', array('local' => $local->getId(), 'barman' => $val->getId())),
                'class'         => 'send-ajax',
                'icon'          => 'icon-key',
                'text'          => 'Enviar Contraseña',
                'confirm_text'  => '¿Estas seguro?',
                'message_title' => 'Contraseña',
                'message_text'  => 'Contraseña enviada correctamente.',
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('client_local_bartender_delete', array('local' => $local->getId(), 'barman' => $val->getId())),
                'class'       => 'del-ajax',
                'icon'        => 'icon-trash',
                'text'        => 'Quitar Permisos',
                'data_reload' => 'reload_client_local_bartender'
            ));

            $array[] = array(
                $val->getNumbers(),
                $val->getUser()->getFullName(),
                $val->getUser()->getRut(),
                $val->getUser()->getEmail(),
                $this->renderView(':Templates:dropdown.html.twig', array(
                    'options' => $options
                ))
            );
        }

        $response = new JsonResponse();
        $response->setContent(json_encode(array('aaData' => $array)));
        return $response;
    }

    /**
     * @Route("/{barman}/send_pass", name="client_local_bartender_send_pass")
     * @param Local $local
     * @param QrScanPermission $barman
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sendPassAction(Local $local, QrScanPermission $barman)
    {
        $em     = $this->getDoctrine()->getManager();
        $helper = $this->get('util.helper');
        $pass   = $helper->randomPass(8);

        $entity = $barman->getUser();
        $entity->setPassword($pass);
        $em->persist($entity);
        $em->flush();

        $this->sendPass($entity, $pass);

        return $this->redirectToRoute('admin_client');
    }

    /**
     * @param User $user
     * @param $pass
     */
    public function sendPass(User $user, $pass)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Pritz - Estos son sus datos de acceso')
            ->setFrom($this->getParameter('mailer_user'))
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'ClientBundle:email:send_pass.html.twig',
                    array(
                        'entity'   => $user,
                        'password' => $pass
                    )
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }
}
