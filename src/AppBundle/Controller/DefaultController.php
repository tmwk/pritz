<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 * @Route("/")
 */
class DefaultController extends FOSRestController
{
    /**
     * @return mixed
     * @Route("/", name="app_home")
     */
    public function indexAction()
    {
        return new Response('App');
    }
}
