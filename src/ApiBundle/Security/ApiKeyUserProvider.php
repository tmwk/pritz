<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 18:33
 */

namespace ApiBundle\Security;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ApiKeyUserProvider implements UserProviderInterface
{
    private $em;
    private $request;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getUsernameForApiKey($apiKey)
    {
        $dq = $this->em->createQueryBuilder()
            ->select('u')
            ->from('ApiBundle:AccessToken', 'u')
            ->where('u.api_key = :apykey')
            ->orWhere('u.ip = :ip')
            ->setParameters(array(
                'apykey' => $apiKey,
                'ip'     => $this->request->getClientIp()
            ))
            ->getQuery()
            ->getResult();
        $user = isset($dq[0])?$dq[0]:false;

//        $user = $this->em->getRepository("ApiBundle:AccessToken")->findOneBy(array('api_key' => $apiKey));
        if ($user) {
            return $user->getName();
        }
        return false;
    }

    public function loadUserByUsername($username)
    {
        return new User(
            $username,
            null,
            array('ROLE_API')
        );
    }

    public function refreshUser(UserInterface $user)
    {
        return $user;
    }

    public function supportsClass($class)
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }

    public function setRequest( ContainerInterface $container ) {

        $this->request = $container->get('request');
    }
}