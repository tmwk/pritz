<?php

namespace ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/', array(), array(), array(
            'CONTENT_TYPE'          => 'application/json',
            'Accept'                => 'application/json',
            'HTTP_REFERER'          => '/foo/bar',
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ));

        $this->assertGreaterThan(0, $crawler->filter('html:contains("Authentication Failed.")')->count());
    }

    public function testIndexAccessToken()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/', array('apikey' => '123456'), array(), array(), 'application/json');

        $this->assertGreaterThan(0, $crawler->filter('html:contains("Welcome")')->count());
    }


}
