<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 04/07/2016
 * Time: 17:41
 */

namespace ApiBundle\Tests\Utilities;


use ApiBundle\Utilities\Helper;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HelperTest extends WebTestCase
{
    public function testGenerateRandom()
    {
        $help   = new Helper();
        $keyspace = '0123456789';
        $result = $help->generateRandom(6, $keyspace);
        $this->assertStringMatchesFormat('%i', $result);
        $this->assertTrue(strlen($result) === 6);
    }

    public function testRandomOnlyNumber() {
        $help   = new Helper();
        $result = $help->randomOnlyNumber(6);

        $this->assertStringMatchesFormat('%i', $result);
        $this->assertTrue(strlen($result) === 6);
    }

    public function testRandomStringLowercase() {
        $help   = new Helper();
        $result = $help->randomStringLowercase(6);

        $this->assertStringMatchesFormat('%S', $result);
        $this->assertTrue(strlen($result) === 6);
    }

    public function testRandomStringUppercase() {
        $help   = new Helper();
        $result = $help->randomStringUppercase(6);

        $this->assertStringMatchesFormat('%S', $result);
        $this->assertTrue(strlen($result) === 6);
    }

    public function testRandomPass() {
        $help   = new Helper();
        $result = $help->randomPass(6);

        $this->assertStringMatchesFormat('%S', $result);
        $this->assertTrue(strlen($result) === 6);
    }

    public function testRandomPassLowercase() {
        $help   = new Helper();
        $result = $help->randomPassLowercase(6);

        $this->assertStringMatchesFormat('%S', $result);
        $this->assertTrue(strlen($result) === 6);
    }

    public function testRandomPassUppercase() {
        $help   = new Helper();
        $result = $help->randomPassUppercase(6);

        $this->assertStringMatchesFormat('%S', $result);
        $this->assertTrue(strlen($result) === 6);
    }


}