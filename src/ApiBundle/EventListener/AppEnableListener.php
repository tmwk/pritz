<?php

/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 24/11/2016
 * Time: 11:22
 */
namespace ApiBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class AppEnableListener
{
    private $em;
    private $host;

    public function __construct(EntityManager $em, $host)
    {
        $this->em   = $em;
        $this->host = $host;
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $request     = $event->getRequest();

        if ($request->getHost() !== $this->host){
            return;
        }

        $config = $this->em->getRepository('ApiBundle:Config')->findOneBy(array());
        if ($config->getAppActive() === true) {
            return;
        }

        $response = new JsonResponse();
        $response->setContent(json_encode(array('error' => array('message' => utf8_encode('La aplicación esta desactivada')))));
        $response->headers->set('Content-Type', 'application/json');

        $event->setResponse($response);
    }
}