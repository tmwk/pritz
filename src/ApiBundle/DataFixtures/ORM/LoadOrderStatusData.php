<?php

/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 12/10/2016
 * Time: 11:28
 */

namespace ApiBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ApiBundle\Entity\OrderStatus;

class LoadOrderStatusData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $status0 = new OrderStatus();
        $status0->setLabel('Pending');
        $manager->persist($status0);

        $status1 = new OrderStatus();
        $status1->setLabel('Paid');
        $manager->persist($status1);

        $status2 = new OrderStatus();
        $status2->setLabel('Retired');
        $manager->persist($status2);

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}