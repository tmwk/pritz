<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\CreditCard;
use ApiBundle\Entity\Payment;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Tmwk\TransbankBundle\Lib\CertificationBag;
use Tmwk\TransbankBundle\Lib\CertificationBagFactory;
use Tmwk\TransbankBundle\Lib\RedirectorHelper;
use Tmwk\TransbankBundle\Lib\TransbankServiceFactory;

/**
 * Class OneClickController
 * @package ApiBundle\Controller
 * @Route("/one_click")
 */
class OneClickController extends FOSRestController
{
    /**
     * @param Request $request
     * @return mixed
     * @Route("/init_inscription", name="one_click_init_inscription")
     */
    public function initInscriptionAction(Request $request)
    {
        $em      = $this->getDoctrine()->getManager();
        $session = $request->getSession();

        $access_token = $request->get('access_token');

        $session->set('access_token', $access_token);

        $user_entity = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        if($this->getParameter('webpay_production')){
            $certificationBag = new CertificationBag(dirname(__FILE__) . '/../Certificados/597020000541.key', dirname(__FILE__) . '/../Certificados/597020000541.crt', null, CertificationBag::PRODUCTION);
        }else{
            $certificationBag = CertificationBagFactory::integrationOneClick();
        }

        $oneClick = TransbankServiceFactory::oneclick($certificationBag);

        $response = $oneClick->initInscription($user_entity->getEmail(), $user_entity->getEmail(), $this->generateUrl('one_click_finish_inscription', array(), true));

        return new Response(RedirectorHelper::redirectHTML($response->urlWebpay, $response->token));
    }

    /**
     * @param Request $request
     * @return mixed
     * @Route("/finish_inscription", name="one_click_finish_inscription")
     */
    public function finishInscriptionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $session = $request->getSession();

        $tbk_token = $request->get('TBK_TOKEN');

        $access_token = $session->get('access_token');

        $user_entity = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        if ($this->getParameter('webpay_production')) {
            $certificationBag = new CertificationBag(dirname(__FILE__) . '/../Certificados/597020000541.key', dirname(__FILE__) . '/../Certificados/597020000541.crt', null, CertificationBag::PRODUCTION);
        } else {
            $certificationBag = CertificationBagFactory::integrationOneClick();
        }

        $oneClick = TransbankServiceFactory::oneclick($certificationBag);

        $response = $oneClick->finishInscription($tbk_token);

        if ($response->responseCode === 0) {

            $credit_card = $em->getRepository('ApiBundle:CreditCard')->findOneBy(array(
                'user'        => $user_entity->getId(),
                'last_digits' => $response->last4CardDigits,
                'is_removed'  => false
            ));

            if (!$credit_card) {
                $card_entity = new CreditCard();
                $card_entity->setAuthCode($response->authCode);
                $card_entity->setLastDigits($response->last4CardDigits);
                $card_entity->setType($response->creditCardType);
                $card_entity->setToken($response->tbkUser);
                $card_entity->setUser($user_entity);
                $card_entity->setIsRemoved(false);
                $card_entity->setCreateDate(new \DateTime('NOW'));
                if ($user_entity->getCreditCards()) {
                    $card_entity->setIsBookmark(true);
                }
                $em->persist($card_entity);
                $em->flush();
            }

            $items = array(
                'authCode'   => $response->authCode,
                'lastDigits' => $response->last4CardDigits,
                'type'       => $response->creditCardType
            );

        } else {
            $items = array(
                'error' => array(
                    'message' => 'Error ' . $response->responseCode
                )
            );
        }

        $session->remove('access_token');

        $view = $this->view($items, 200)
            ->setTemplate(':webpay:result.html.twig')
            ->setTemplateData(array('items' => $items));
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $order_id
     * @return mixed
     * @Route("/authorize/{order_id}", name="one_click_authorize")
     */
    public function authorizeAction(Request $request, $order_id)
    {
        $em      = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $helper = $this->get('util.helper');
        $order_controller = new OrderController();

        $order_entity = $em->getRepository('ApiBundle:Order')->find($order_id);
        $access_token = $order_entity->getUser()->getAccessToken();
        $user_entity  = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        if ($this->getParameter('webpay_production')) {
            $certificationBag = new CertificationBag(dirname(__FILE__) . '/../Certificados/597020000541.key', dirname(__FILE__) . '/../Certificados/597020000541.crt', null, CertificationBag::PRODUCTION);
        } else {
            $certificationBag = CertificationBagFactory::integrationOneClick();
        }

        $oneClick = TransbankServiceFactory::oneclick($certificationBag);

        $credit_card = $em->getRepository('ApiBundle:CreditCard')->find($order_entity->getCreditCard()->getId());

        $response = $oneClick->authorize($order_entity->getTotal(), time(), $user_entity->getEmail(), $credit_card->getToken());

        if ($response->responseCode === 0) {
            $pay_entity = new Payment();
            $pay_entity->setType(Payment::TYPE_WEBPAY_ONECLICK);
            $pay_entity->setAuthorizationCode($response->authorizationCode);
            $pay_entity->setCreditCardType($response->creditCardType);
            $pay_entity->setLastCardDigits($response->last4CardDigits);
            $pay_entity->setResponseCode($response->responseCode);
            $pay_entity->setTransactionId($response->transactionId);
            $pay_entity->setCreateDate(new \DateTime('NOW'));
            $pay_entity->setOrder($order_entity);
            $em->persist($pay_entity);
            $em->flush();

            $status_entity = $em->getRepository('ApiBundle:OrderStatus')->findOneBy(array('label' => 'Paid'));
            $order_update  = $em->getRepository('ApiBundle:Order')->find($order_id);
            $order_update->setStatus($status_entity);
            $order_update->setWithdrawnCode($helper->randomOnlyNumber(30, $order_update->getId()));
            $em->persist($order_update);
            $em->flush();


            $items = $order_controller->orderInfo($request, $order_entity);
        } else {
            $items = array(
                'error' => array(
                    'message' => 'Error ' . $response->responseCode
                )
            );
        }

        $session->remove('access_token');

        $view = $this->view($items, 200)
            ->setTemplate(':webpay:result.html.twig')
            ->setTemplateData(array('items' => $items));
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $card_id
     * @internal param $order_id
     * @Route("/remove/{card_id}", name="one_click_remove")
     * @return Response
     */
    public function removeUser(Request $request, $card_id)
    {
        $em      = $this->getDoctrine()->getManager();
        $session = $request->getSession();

        $access_token = $request->get('access_token');

        $session->set('access_token', $access_token);

        $user_entity = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));
        $card_entity = $em->getRepository('ApiBundle:CreditCard')->find($card_id);

        if ($this->getParameter('webpay_production')) {
            $certificationBag = new CertificationBag(dirname(__FILE__) . '/../Certificados/597020000541.key', dirname(__FILE__) . '/../Certificados/597020000541.crt', null, CertificationBag::PRODUCTION);
        } else {
            $certificationBag = CertificationBagFactory::integrationOneClick();
        }

        $oneClick = TransbankServiceFactory::oneclick($certificationBag);

        $response = $oneClick->removeUser($card_entity->getToken(), $user_entity->getEmail());

        if ($response === true) {

            $card_entity->setIsRemoved(true);
            $em->persist($card_entity);
            $em->flush();

            $first_card = $em->getRepository('ApiBundle:CreditCard')->findOneBy(array('user' => $user_entity->getId(), 'is_removed' => false));
            if($first_card){
                return $this->forward('ApiBundle:CreditCard:selectAsFavorite', array('card_id' => $first_card->getId()));
            }


            $items = array(
                'result' => true
            );
        } else {
            $items = array(
                'error' => array(
                    'message' => "No se pudo eliminar la tarjeta #{$card_id}."
                )
            );
        }

        $view = $this->view($items, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $items));
        return $this->handleView($view);
    }


}
