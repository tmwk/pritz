<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Order;
use ApiBundle\Entity\OrderItem;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderController
 * @package ApiBundle\Controller
 * @Route("/order")
 */
class OrderController extends FOSRestController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="order")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $access_token = $request->get('access_token');
        $user_entity  = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        $order = array();
        foreach ($user_entity->getOrders() as $key => $orders) {
            if ($orders->getStatus()->getLabel() === 'Pending') {
                continue;
            }

            $order[] = $this->orderInfo($request, $orders);
        }

        $view = $this->view($order, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $order));
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @Route("/debug", name="order_debug")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function debugAction(Request $request){

        if ($request->isMethod("post")){

            return $this->forward("ApiBundle:Order:new", array());

        }

        $view = $this->view(null, 200)
            ->setTemplate(':order:debug.html.twig');
        return $this->handleView($view);

    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/{id}", name="order_show")
     * @Method("GET")
     */
    public function showAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $access_token = $request->get('access_token');
        $user_entity  = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        $orders = $em->getRepository('ApiBundle:Order')->findOneBy(array('id' => $id));

        $localPermission = $em->getRepository('ApiBundle:QrScanPermission')->findOneBy(array('local' => $orders->getLocal()->getId(), 'user' => $user_entity->getId()));

        if(($user_entity->getId() === $orders->getUser()->getId()) || $localPermission){
            $order[0] = $this->orderInfo($request, $orders);
        }else{
            $order = array();
        }

        $view = $this->view($order, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $order));
        return $this->handleView($view);
    }

    /**
     * @param $request
     * @param $orders
     * @return mixed
     */
    public function orderInfo($request, $orders)
    {
        $order['id']          = $orders->getId();
        $order['user']        = array(
            'id'        => $orders->getUser()->getId(),
            'firstname' => $orders->getUser()->getFirstname(),
            'lastname'  => $orders->getUser()->getLastname(),
        );
        if($orders->getBarman()){
            $order['barman']        = array(
                'id'        => $orders->getBarman()->getId(),
                'firstname' => $orders->getBarman()->getFirstname(),
                'lastname'  => $orders->getBarman()->getLastname(),
            );
        }
        $order['local']       = array(
            'id'         => $orders->getLocal()->getId(),
            'name'       => $orders->getLocal()->getName(),
            'latitude'   => $orders->getLocal()->getLatitude(),
            'longitude'  => $orders->getLocal()->getLongitude(),
            'statusText' => $orders->getLocal()->getStatusText(),
            'url_img'    => $request->getScheme() . '://' . $request->getHttpHost() . '/images/local/' . $orders->getLocal()->getId() . '/' . $orders->getLocal()->getImageFilename(),
            'event_type' => array(
                'ico_filename' => $orders->getLocal()->getEventType()->getIconFilename(),
                'label'        => $orders->getLocal()->getEventType()->getLabel(),
            ),
        );
        $order['create_date'] = date_format($orders->getCreateDate(), 'c');

        foreach ($orders->getOrderItem() as $item) {
            $order['items'][] = array(
                'id'       => $item->getId(),
                'name'     => $item->getProduct()->getName(),
                'quantity' => $item->getQuantity(),
                'price'    => $item->getUnitPrice(),
            );
        }

        $order['total'] = $orders->getTotal();

        $order['status'] = $orders->getStatus()->getLabel();

        if ($orders->getStatus()->getLabel() === 'Retired') {
            if ($orders->getWithdrawnDate() !== null) {
                $order['withdrawn'] = date_format($orders->getWithdrawnDate(), 'c');
            } else {
                $order['withdrawn'] = false;
            }
        } else {
            $order['withdrawn'] = false;
        }

        if ($orders->getWithdrawnCode() !== null) {
            if ($orders->getStatus()->getLabel() === 'Retired') {
                $order['withdrawn_code'] = false;
            } else {
                $order['withdrawn_code'] = $orders->getWithdrawnCode();
            }

        } else {
            $order['withdrawn_code'] = false;
        }

        return $order;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/new", name="order_new")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        $em      = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $hh      = $this->get('util.happy_hour');

        $access_token   = $request->get('access_token');
        $local_id       = $request->get('local_id');
        $credit_card_id = $request->get('credit_card_id', null);
        $products       = $request->get('products');
        $happy_hour     = $request->get('happy_hour') === 'true' ? true : false;
        $items          = '';

        $session->set('access_token', $access_token);

        if ($happy_hour === $hh->check($local_id)) {

            $user_entity        = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));
            $local_entity       = $em->getRepository('ApiBundle:Local')->find($local_id);
            $credit_card_entity = is_null($credit_card_id) ? null : $em->getRepository('ApiBundle:CreditCard')->find($credit_card_id);
            $status_entity      = $em->getRepository('ApiBundle:OrderStatus')->findOneBy(array('label' => 'Pending'));

            if ($local_entity) {

                $order = new Order();
                $order->setSalt(time());
                $order->setCreditCard($credit_card_entity);
                $order->setUser($user_entity);
                $order->setCreateDate(new \DateTime('NOW'));
                $order->setLocal($local_entity);
                $order->setTotal(0);
                $order->setStatus($status_entity);
                $order->setWithdrawnDate(null);
                $em->persist($order);
                $em->flush();

                $products = explode('|', $products);
                if (count($products) !== 0) {
                    $amount = 0;
                    foreach ($products as $product) {
                        list($id, $quanty) = explode('-', $product);

                        $prod  = $em->getRepository('ApiBundle:Product')->find($id);
                        $price = $hh->check($local_id) ? $prod->getPriceHh() : $prod->getPrice();

                        $order_item = new OrderItem();
                        $order_item->setOrder($order);
                        $order_item->setProduct($prod);
                        $order_item->setQuantity($quanty);
                        $order_item->setUnitPrice($price);
                        $em->persist($order_item);

                        $amount += $price * $quanty;
                    }

                    $order_update = $em->getRepository('ApiBundle:Order')->find($order->getId());
                    $order_update->setTotal($amount);
                    $em->persist($order_update);

                    $em->flush();

                    $forward_action = is_null($credit_card_entity) ? 'ApiBundle:Webpay:init' : 'ApiBundle:OneClick:authorize';

                    return $this->forward($forward_action, array('order_id' => $order->getId()));

                } else {
                    $items = array('error' => array('message' => 'La orden no tiene productos'));
                }
            } else {
                $items = array('error' => array('message' => 'El local no existe.'));
            }

        } else {

            if ($happy_hour === true && $hh->check($local_id) === false) {
                $items = array('error' => array('message' => 'El Happy Hour a finalizado durante la compra'));
            } elseif ($happy_hour === false && $hh->check($local_id) === true) {
                $items = array('error' => array('message' => 'El Happy Hour a comenzado durante la compra'));
            }

        }

        $view = $this->view($items, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $items));
        return $this->handleView($view);
    }

    /**
     * @Route("/withdrawn/{code}")
     * @param Request $request
     * @param $code
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function withdrawnAction(Request $request, $code)
    {
        $em            = $this->getDoctrine()->getManager();
        $access_token  = $request->get('access_token');

        $user_entity   = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));
        $order_entity  = $em->getRepository('ApiBundle:Order')->findOneBy(array('withdrawn_code' => $code));
        $status_entity = $em->getRepository('ApiBundle:OrderStatus')->findOneBy(array('label' => 'Retired'));

        $permission = $em->getRepository('ApiBundle:QrScanPermission')->findOneBy(array('user' => $user_entity->getId(), 'local' => $order_entity->getLocal()->getId()));



        if($permission){
            if ($order_entity) {

                if ($order_entity->getStatus()->getLabel() !== 'Retired') {
                    $order_entity->setBarman($user_entity);
                    $order_entity->setStatus($status_entity);
                    $order_entity->setWithdrawnDate(new \DateTime('NOW'));
                    $em->persist($order_entity);
                    $em->flush();

                    $items = array(
                        'id'     => $order_entity->getId(),
                        'result' => true
                    );
                } else {
                    $items = array('error' => array('id' => $order_entity->getId(), 'message' => 'La orden ya fue canjeada.'));
                }

            } else {
                $items = array('error' => array('id' => $order_entity->getId(), 'message' => 'La orden no existe.'));
            }
        }else{
            $items = array('error' => array('id' => $order_entity->getId(), 'message' => 'No tienes los permisos suficientes.'));
        }



        $view = $this->view($items, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $items));
        return $this->handleView($view);
    }
}
