<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 18:13
 */

namespace ApiBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoginController
 * @package ApiBundle\Controller
 * @Route("/")
 */
class LoginController extends FOSRestController
{
    /**
     * @Route("/login", name="login")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method("POST")
     */
    public function loginAction(Request $request)
    {
        $em     = $this->getDoctrine()->getManager();
        $helper = $this->get('util.helper');

        $fbuid      = $request->request->get('fbuid');
        $twuid      = $request->request->get('twuid');
        $post_email = $request->request->get('email');
        $post_pass  = $request->request->get('password');

        if ($fbuid) {
            $param = array('fb_uid' => $fbuid);
        } elseif ($twuid) {
            $param = array('tw_uid' => $twuid);
        } else {
            $param = array('email' => $post_email);
        }

        $user = $em->getRepository('ApiBundle:User')->findOneBy($param);

        if ($user && (($user->getPassword() === md5($post_pass) || ($user->getFbUid() && $user->getFbUid() == $fbuid) || ($user->getTwUid() && $user->getTwUid() == $twuid)))) {

            $dateNow = new \DateTime('now');
            $user->setAccessToken($helper->randomPass(32));
            $user->setLastLogin($dateNow);
            $em->persist($user);
            $em->flush();

            $result = array(
                'user' => array(
                    'id'            => $user->getId(),
                    'firstname'     => $user->getFirstname(),
                    'lastname'      => $user->getLastname(),
                    'birthdate'     => $user->getBirthdate() ? date_format($user->getBirthdate(), 'c') : null,
                    'rut'           => $user->getRut(),
                    'email'         => $user->getEmail(),
                    'has_password'  => !is_null($user->getPassword()),
                    'can_scan_qr'   => $user->getQrScanPermissions()->count() > 0,
                    'register_date' => $user->getRegisterDate() ? date_format($user->getRegisterDate(), 'c') : null,
                    'last_update'   => $user->getLastUpdate() ? date_format($user->getLastUpdate(), 'c') : null,
                    'last_login'    => $user->getLastLogin() ? date_format($user->getLastLogin(), 'c') : null,
                    'access_token'  => $user->getAccessToken(),
                )
            );
        } else {
            $result = array(
                'error' => array(
                    'message' => 'Información incorrecta.'
                )
            );
        }

        $view = $this->view($result, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $result));
        return $this->handleView($view);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {

    }

    /**
     * @param Request $request
     * @Route("change_password", name="change_password")
     * @Method("POST")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changePasswordAction(Request $request)
    {
        $em           = $this->getDoctrine()->getManager();
        $access_token = $request->request->get('access_token');
        $old_pass     = $request->request->get('old_password');
        $new_pass     = $request->request->get('new_password');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        if($user->getPassword() === md5($old_pass)){
            $user->setPassword($new_pass);
            $em->persist($user);
            $em->flush();
            $result = array();
        }else{
            $result = array(
                'error' => array(
                    'message' => 'Información incorrecta.'
                )
            );
        }

        $view = $this->view($result, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $result));
        return $this->handleView($view);
    }

    /**
     * @Route("recover_password", name="recover_password")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recoverPasswordAction(Request $request)
    {
        $em     = $this->getDoctrine()->getManager();
        $helper = $this->get('util.helper');

        $email   = $request->request->get('email');
        $forward = $request->request->get('forward');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array('email' => $email));

        $newSecretCode = $forward ? $user->getSecretCode() : $helper->randomOnlyNumber(6);

        if ($user) {
            $user->setSecretCode($newSecretCode);
            $em->persist($user);
            $em->flush();
            $message = \Swift_Message::newInstance()
                ->setSubject('Recuperar contraseña: ' . $user->getSecretCode())
                ->setFrom($this->container->getParameter('mailer_user'))
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                        ':emails:recover_password.html.twig',
                        array(
                            'recover_code' => $user->getSecretCode()
                        )
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            $result = array(
                'email' => $user->getEmail(),
                'code'  => $user->getSecretCode(),
            );
        } else {
            $result = array(
                'error' => array(
                    'message' => 'Información incorrecta.'
                )
            );
        }

        $view = $this->view($result, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $result));
        return $this->handleView($view);
    }

    /**
     * @Route("recover_change_password", name="recover_change_password")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recoverChangePasswordAction(Request $request)
    {
        $em    = $this->getDoctrine()->getManager();
        $email = $request->request->get('email');
        $code  = $request->request->get('secret_code');
        $pass  = $request->request->get('password');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array('email' => $email, 'secret_code' => $code));

        if ($user && $user->getSecretCode() !== null) {
            $user->setPassword($pass);
            $user->setSecretCode(null);
            $em->persist($user);
            $em->flush();

            $result = array();
        } else {
            $result = array(
                'error' => array(
                    'message' => 'Información incorrecta.'
                )
            );
        }

        $view = $this->view($result, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $result));
        return $this->handleView($view);
    }


}