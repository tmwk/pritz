<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\ProductBookmark;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 * @package ApiBundle\Controller
 * @Route("/product")
 */
class ProductController extends FOSRestController
{
    /**
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/")
     */
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    /**
     * @param Request $request
     * @param $product_id
     * @internal param $params
     * @internal param array $options
     * @Route("/select_as_favorite/{product_id}")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function selectAsFavoriteAction(Request $request, $product_id)
    {
        $em = $this->getDoctrine()->getManager();

        $access_token = $request->get('access_token');

        $user_entity = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        $product_entity = $em->getRepository('ApiBundle:Product')->find($product_id);

        $product_bookmark = $em->getRepository('ApiBundle:ProductBookmark')->findOneBy(array('user' => $user_entity->getId(), 'product' => $product_id));

        if(!$product_bookmark){
            $bookmark = new ProductBookmark();
            $bookmark->setUser($user_entity);
            $bookmark->setProduct($product_entity);
            $bookmark->setCreateDate(new \DateTime('now'));
            $em->persist($bookmark);
            $em->flush();

            $item = array(
                'result' => true
            );
        }else{
            $item = array(
                'error' => array(
                    'message' => 'El producto ya es favorito.'
                )
            );
        }

        $view = $this->view($item, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $item));
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $product_id
     * @internal param $params
     * @internal param array $options
     * @Route("/remove_as_favorite/{product_id}")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeAsFavoriteAction(Request $request, $product_id)
    {
        $em = $this->getDoctrine()->getManager();

        $access_token = $request->get('access_token');

        $user_entity = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        $bookmark = $em->getRepository('ApiBundle:ProductBookmark')->findOneBy(array('user' => $user_entity->getId(), 'product' => $product_id));

        if($bookmark){
            $em->remove($bookmark);
            $em->flush();
            $item = array(
                'result' => true
            );
        }else{
            $item = array(
                'error' => array(
                    'message' => 'El producto no es favorito'
                )
            );
        }

        $view = $this->view($item, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $item));
        return $this->handleView($view);
    }
}
