<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 11/05/2016
 * Time: 17:58
 */

namespace ApiBundle\Controller;


use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LocalController
 * @package ApiBundle\Controller
 * @Route("/local")
 */
class LocalController extends FOSRestController
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="local")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em           = $this->getDoctrine()->getManager();
        $locals       = $em->getRepository('ApiBundle:Local')->findBy(array('enabled' => true));
        $access_token = $request->query->get('access_token');
        $user         = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));
        $result       = array();

        foreach ($locals as $local) {
            $monthlyPurchasesLocal = $em->getRepository('ApiBundle:Order')->monthlyFromLocalTotal($local->getId());
            $purchasesLocalUser    = $em->getRepository('ApiBundle:Order')->fromLocalAndUserTotal($local->getId(), $user->getId());

            $result['locals'][] = array(
                'id'                         => $local->getId(),
                'name'                       => $local->getName(),
                'latitude'                   => $local->getLatitude(),
                'longitude'                  => $local->getLongitude(),
                'statusText'                 => $local->getStatusText(),
                'event_type'                 => array(
                    'ico_filename' => $local->getEventType()->getIconFilename(),
                    'label'        => $local->getEventType()->getLabel(),
                ),
                'monthlyPurchasesLocalCount' => $monthlyPurchasesLocal,
                'purchasesLocalUserCount'    => $purchasesLocalUser,
            );
        }

        $view = $this->view($result, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $result));
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/{id}", name="local_show")
     * @Method("GET")
     */
    public function showAction(Request $request, $id)
    {
        $em             = $this->getDoctrine()->getManager();
        $access_token   = $request->get('access_token');
        $user_entity    = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));
        $bookmark_only  = $request->get('bookmark_only', 'false') == 'true';

        $local = $em->getRepository('ApiBundle:Local')->find($id);

        $result['local'] = array(
            'id'      => $local->getId(),
            'name'    => $local->getName(),
            'url_img' => $request->getScheme() . '://' . $request->getHttpHost() . '/images/local/' . $local->getId() . '/' . $local->getImageFilename(),
        );

        foreach ($local->getProducts() as $product) {

            $is_bookmark = function () use ($em, $user_entity, $product) {
                $bookmark = $em->getRepository('ApiBundle:ProductBookmark')->findOneBy(array('user' => $user_entity->getId(), 'product' => $product->getId()));
                return $bookmark ? true : false;
            };

            if ($bookmark_only && !$is_bookmark())  continue;

            $result['categories'][$product->getCategory()->getId()]['name']       = $product->getCategory()->getName();
            $result['categories'][$product->getCategory()->getId()]['id']         = $product->getCategory()->getId();
            $result['categories'][$product->getCategory()->getId()]['products'][] = array(
                'id'          => $product->getId(),
                'name'        => $product->getName(),
                'description' => $product->getDescription(),
                'price'       => $product->getPrice(),
                'price_hh'    => $product->getPriceHh(),
                'create_date' => date_format($product->getCreateDate(), 'c'),
                'last_update' => date_format($product->getLastUpdate(), 'c'),
                'category'    => array(
                    'id'   => $product->getCategory()->getId(),
                    'name' => $product->getCategory()->getName()
                ),
                'tax'         => array(
                    'name'  => $product->getTaxType()->getLabel(),
                    'value' => $product->getTaxType()->getValue(),
                ),
                'is_bookmark' => $is_bookmark(),
            );


        }

        if (array_key_exists('categories', $result)) {
            $result['categories'] = array_values($result['categories']);
        }


        $view = $this->view($result, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $result));
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/last_order/{local_id}", name="local_last_order")
     */
    public function orderLastAction(Request $request, $local_id)
    {
        $em = $this->getDoctrine()->getManager();

        $access_token = $request->get('access_token');
        $limit        = $request->get('limit') ? $request->get('limit') : 20;

        $orderController = new OrderController();
        $order           = array();

        $user_entity = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        $status_entity = $em->getRepository('ApiBundle:OrderStatus')->findOneBy(array('label' => 'Retired'));

        $orders = $em->getRepository('ApiBundle:Order')->findBy(array('local' => $local_id, 'status' => $status_entity->getId()), array('withdrawn_date' => 'desc'), $limit);

        foreach ($orders as $key => $ord) {
            $order[$key] = $orderController->orderInfo($request, $ord);
        }

        $view = $this->view($order, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $order));
        return $this->handleView($view);
    }
}