<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 06/05/2016
 * Time: 13:34
 */

namespace ApiBundle\Controller;

use ApiBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RegisterController
 * @package ApiBundle\Controller
 * @Route("/")
 */
class RegisterController extends FOSRestController
{
    /**
     * @Route("/register")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method("POST")
     */
    public function indexAction(Request $request)
    {
        $em  = $this->getDoctrine()->getManager();
        $rut = $this->get('util.rut_validator');

        $fbuid          = $request->request->get('fbuid');
        $twuid          = $request->request->get('twuid');
        $post_firstname = $request->request->get('firstname');
        $post_lastname  = $request->request->get('lastname');
        $post_birthdate = $request->request->get('birthdate');
        $post_rut       = $request->request->get('rut');
        $post_email     = $request->request->get('email');
        $post_password  = $request->request->get('password');

        $unique_email = $em->getRepository('ApiBundle:User')->findOneBy(array('email' => $post_email));

        $errors = array();

        if (strlen($post_firstname) == 0) {
            $errors['firstname'] = 'Ingrese sus nombres';
        }
//        if (strlen($post_lastname) == 0) {
//            $errors['lastname'] = 'Ingrese sus apellidos';
//        }
        if (!empty($post_rut) && !$rut::valida_rut($post_rut)) {
            $errors['rut'] = 'Ingrese un rut valido';
        }
        if (!empty($post_email) && !filter_var($post_email, FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = 'Ingrese un e-mail valido';
        } elseif (!empty($post_email) && $unique_email) {
            $errors['email'] = 'El e-mail ya esta registrado';
        }

        if (count($errors) === 0) {
            $user           = new User();
            $date_birthdate = new \DateTime($post_birthdate);
            $birthdate      = new \DateTime($date_birthdate->format('Y-m-d'));
            $dateNow        = new \DateTime('now');
            $user->setFbUid($fbuid);
            $user->setTwUid($twuid);
            $user->setFirstname($post_firstname);
            $user->setLastname($post_lastname);
            $user->setBirthdate($post_birthdate ? $birthdate : null);
            $user->setRut($post_rut);
            $user->setEmail($post_email);

            if (strlen($post_password) > 0)
                $user->setPassword($post_password);

            $user->setRegisterDate($dateNow);
            $user->setLastUpdate($dateNow);
            $em->persist($user);
            $em->flush();

            $result = array(
                'user' => array(
                    'firstname'     => $user->getFirstname(),
                    'lastname'      => $user->getLastname(),
                    'birthdate'     => $user->getBirthdate() ? date_format($user->getBirthdate(), 'c') : null,
                    'rut'           => $user->getRut(),
                    'email'         => $user->getEmail(),
                    'has_password'  => !is_null($user->getPassword()),
                    'can_scan_qr'   => $user->getQrScanPermissions()->count() > 0,
                    'register_date' => $user->getRegisterDate() ? date_format($user->getRegisterDate(), 'c') : null,
                    'last_update'   => $user->getLastUpdate() ? date_format($user->getLastUpdate(), 'c') : null,
                    'last_login'    => $user->getLastLogin() ? date_format($user->getLastLogin(), 'c') : null,
                    'access_token'  => $user->getAccessToken(),
                )
            );
        } else {
            $result = array(
                'errors' => $errors
            );
        }

        $view = $this->view($result, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $result));
        return $this->handleView($view);
    }
}