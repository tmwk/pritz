<?php

namespace ApiBundle\Controller;

use ApiBundle\ApiBundle;
use ApiBundle\Entity\Order;
use ApiBundle\Entity\Payment;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Tmwk\TransbankBundle\Lib\CertificationBag;
use Tmwk\TransbankBundle\Lib\CertificationBagFactory;
use Tmwk\TransbankBundle\Lib\Logger;
use Tmwk\TransbankBundle\Lib\RedirectorHelper;
use Tmwk\TransbankBundle\Lib\TransbankServiceFactory;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;

/**
 * Class WebpayController
 * @package ApiBundle\Controller
 * @Route("/webpay")
 */
class WebpayController extends FOSRestController
{

    /**
     * @param Request $request
     * @param $order_id
     * @return mixed
     * @Route("/init/{order_id}", name="webpay_init")
     */
    public function initAction(Request $request, $order_id)
    {
        $log     = Logger::log();
        $em      = $this->getDoctrine()->getManager();
        $session = $request->getSession();

        $log->info('================ Orden #' . $order_id . ' ================');

        $access_token = $request->get('access_token');

        $session->set('access_token', $access_token);

        $user_entity = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        /**
         * @var Order $order_entity
         */
        $order_entity = $em->getRepository('ApiBundle:Order')->find($order_id);

        if ($this->getParameter('webpay_production')) {
            $certificationBag = new CertificationBag(dirname(__FILE__) . '/../Certificados/597020000541.key', dirname(__FILE__) . '/../Certificados/597020000541.crt', null, CertificationBag::PRODUCTION);
        } else {
            $certificationBag = CertificationBagFactory::integrationWebpayNormal();
        }

        $normal = TransbankServiceFactory::normal($certificationBag);

        $normal->addTransactionDetail($order_entity->getTotal(), $order_entity->getId());

        $response = $normal->initTransaction(
            $this->generateUrl("webpay_response", array(), true),
            $this->generateUrl("webpay_thanks", array(), true)
        );

        $items = array(
            'html' => RedirectorHelper::redirectHTML($response->url, $response->token)
        );

        $view = $this->view($items, 200)
            ->setTemplate(':default:variable.html.twig')
            ->setTemplateData(array('variable' => $items['html']));
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @return mixed
     * @Route("/response", name="webpay_response")
     */
    public function responseAction(Request $request){

        $items = null;

        $token_ws = $request->get('token_ws', null);

        if (!is_null($token_ws)){

            if ($this->getParameter('webpay_production')) {
                $certificationBag = new CertificationBag(dirname(__FILE__) . '/../Certificados/597020000541.key', dirname(__FILE__) . '/../Certificados/597020000541.crt', null, CertificationBag::PRODUCTION);
            } else {
                $certificationBag = CertificationBagFactory::integrationWebpayNormal();
            }

            $normal = TransbankServiceFactory::normal($certificationBag);

            $response = $normal->getTransactionResult($token_ws);

            $em             = $this->getDoctrine()->getManager();
            $order_entity   = $em->getRepository('ApiBundle:Order')->find($response->buyOrder);

            if (!is_null($order_entity) && $order_entity->getTotal() == $response->detailOutput->amount){

                //$create_date = \DateTime::createFromFormat('yyyy-MM-dd\'T\'HH:mm:ssZ', $response->accountingDate);

                $payment_entity = new Payment();
                $payment_entity->setType(Payment::TYPE_WEBPAY_NORMAL);
                $payment_entity->setOrder($order_entity);
                $payment_entity->setAuthorizationCode($response->detailOutput->authorizationCode);
                $payment_entity->setLastCardDigits($response->cardDetail->cardNumber);
                $payment_entity->setPaymentTypeCode($response->detailOutput->paymentTypeCode);
                $payment_entity->setResponseCode($response->detailOutput->responseCode);
                $payment_entity->setSharesNumber($response->detailOutput->sharesNumber);
                $payment_entity->setVci($response->VCI);
                $payment_entity->setToken($token_ws);
                $payment_entity->setAccountingDate($response->accountingDate);
                $payment_entity->setCreateDate(new \DateTime('NOW'));

                $em->persist($payment_entity);
                $em->flush();

                $normal->acknowledgeTransaction($token_ws);

                return new Response(RedirectorHelper::redirectBackNormal($response->urlRedirection));
            }
            else{
                $items = array(
                    'error' => array(
                        'message' => 'No se pudo verificar el pago'
                    )
                );
            }
        }
        else{
            $items = array(
                'error' => array(
                    'message' => 'Token webpay faltante'
                )
            );
        }

        $view = $this->view($items, 200)
            ->setTemplate(':webpay:result.html.twig')
            ->setTemplateData(array('items' => $items));
        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @return mixed
     * @Route("/thanks", name="webpay_thanks")
     */
    public function thanksAction(Request $request){

        $items = null;

        $token_ws = $request->get('token_ws', null);

        if (!is_null($token_ws)){

            $em             = $this->getDoctrine()->getManager();
            $payment_entity = $em->getRepository('ApiBundle:Payment')->findOneBy(array('token' => $token_ws));

            if (!is_null($payment_entity)){

                $helper = $this->get('util.helper');

                $status_entity = $em->getRepository('ApiBundle:OrderStatus')->findOneBy(array('label' => 'Paid'));
                $order_update = $payment_entity->getOrder();
                $order_update->setStatus($status_entity);
                $order_update->setWithdrawnCode($helper->randomOnlyNumber(30, $order_update->getId()));
                $em->persist($order_update);
                $em->flush();

                $orderController = new OrderController();
                $orderInfo = $orderController->orderInfo($request, $order_update);

                $items = array(
                    'order' => $orderInfo
                );

            }
            else{
                $items = array(
                    'error' => array(
                        'message' => 'Pago no encontrado'
                    )
                );
            }

        }else{
            $items = array(
                'error' => array(
                    'message' => 'Token webpay faltante'
                )
            );
        }

        $view = $this->view($items, 200)
            ->setTemplate(':webpay:result.html.twig')
            ->setTemplateData(array('items' => $items));
        return $this->handleView($view);
    }
}
