<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Tmwk\TransbankBundle\Lib\CertificationBagFactory;
use Tmwk\TransbankBundle\Lib\RedirectorHelper;
use Tmwk\TransbankBundle\Lib\TransbankServiceFactory;

/**
 * Class DefaultController
 * @package ApiBundle\Controller
 * @Route("/")
 */
class DefaultController extends FOSRestController
{
    /**
     * @return mixed
     * @Route("/", name="home", methods={"GET"})
     */
    public function indexAction()
    {
        $certificationBag = CertificationBagFactory::integrationOneClick();

        $oneClick = TransbankServiceFactory::oneclick($certificationBag);

        $return_url = 'https://localhost/TransBank/result.php?action=';

        $response = $oneClick->initInscription('username', 'mfigueroa@tmwk.cl', $return_url.'finish');

//        echo RedirectorHelper::redirectHTML($response->urlWebpay, $response->token);


        $items[] = array(
            'error' => array(
                'message' => 'undefined'
            )
        );

        $view = $this->view($items, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $items));
        return $this->handleView($view);
    }
}
