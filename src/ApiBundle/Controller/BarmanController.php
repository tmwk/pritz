<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BarmanController
 * @package ApiBundle\Controller
 * @Route("/barman")
 */
class BarmanController extends FOSRestController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/order/last", name="barman_last_order")
     */
    public function orderLastAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $access_token    = $request->get('access_token');
        $orderController = new OrderController();
        $order           = array();

        $user_entity = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        foreach ($user_entity->getOrdersReceived() as $key => $orders) {
            $order[$key] = $orderController->orderInfo($request, $orders);
        }

        $view = $this->view($order, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $order));
        return $this->handleView($view);
    }

    /**
     * @Route("/scan_permissions", name="barman_scan_permissions")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function scanPermissions(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $access_token = $request->get('access_token');
        $user_entity  = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        $items = array();

        if($user_entity){
            foreach ($user_entity->getQrScanPermissions() as $permission) {
                $items[] = array(
                    'id'      => $permission->getLocal()->getId(),
                    'name'    => $permission->getLocal()->getName(),
                    'url_img' => $request->getScheme() . '://' . $request->getHttpHost() . '/images/local/' . $permission->getLocal()->getId() . '/' . $permission->getLocal()->getImageFilename(),
                );
            }
        }else{
            $items = array('error' => array('message' => 'Usuario no tiene locales asignados'));
        }

        $view = $this->view($items, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $items));
        return $this->handleView($view);
    }
}
