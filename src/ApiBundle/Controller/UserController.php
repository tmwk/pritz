<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 06/05/2016
 * Time: 17:52
 */

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class UserController
 * @package ApiBundle\Controller
 * @Route("/user")
 */
class UserController extends FOSRestController
{
    /**
     * @Route("/", name="user")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('ApiBundle:User')->findAll();

        $result = array();
        foreach ($users as $user) {
            $result[] = array(
                'user' => array(
                    'firstname'     => $user->getFirstname(),
                    'lastname'      => $user->getLastname(),
                    'birthdate'     => date_format($user->getBirthdate(), 'd/m/Y'),
                    'rut'           => $user->getRut(),
                    'email'         => $user->getEmail(),
                    'has_password'  => !is_null($user->getPassword()),
                    'can_scan_qr'   => $user->getQrScanPermissions()->count() > 0,
                    'register_date' => date_format($user->getRegisterDate(), 'd/m/Y '),
                    'last_update'   => date_format($user->getLastUpdate(), 'd/m/Y'),
                    'last_login'    => date_format($user->getLastLogin(), 'd/m/Y'),
                    'access_token'  => $user->getAccessToken(),
                )
            );
        }

        $view = $this->view($result, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $result));
        return $this->handleView($view);
    }



}