<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 04/10/2016
 * Time: 17:40
 */

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CreditCardController
 * @package ApiBundle\Controller
 * @Route("/credit_card")
 */
class CreditCardController extends FOSRestController
{
    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @Route("/{id}/orders")
     */
    public function ordersAction(Request $request, $id)
    {
        $em              = $this->getDoctrine()->getManager();
        $orderController = new OrderController();

        $access_token = $request->get('access_token');

        $user_entity = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        $orders = $em->getRepository('ApiBundle:CreditCard')->findOneBy(array('id' => $id, 'user' => $user_entity->getId()));

        $order = array();
        foreach ($orders->getOrders() as $key => $orders) {
            if ($orders->getStatus()->getLabel() === 'Pending') {
                continue;
            }

            $order[] = $orderController->orderInfo($request, $orders);

        }

        $view = $this->view($order, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $order));
        return $this->handleView($view);
    }


    /**
     * @param Request $request
     * @param $card_id
     * @Route("/select_as_favorite/{card_id}")
     * @return Response
     */
    public function selectAsFavoriteAction(Request $request, $card_id)
    {
        $em      = $this->getDoctrine()->getManager();
        $session = $request->getSession();

        if ($session->has('access_token')) {
            $access_token = $session->get('access_token');
        } else {
            $access_token = $request->get('access_token');
        }

        $user_entity = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        foreach ($user_entity->getCreditCards() as $card) {
            if ($card_id == $card->getId()) {
                $card->setIsBookmark(true);
            } else {
                $card->setIsBookmark(false);
            }


            $em->persist($card);
        }
        $em->flush();

        $items = array(
            'result' => true
        );

        $view = $this->view($items, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $items));
        return $this->handleView($view);

    }

    /**
     * @Route("/list", name="credit_card_list")
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine();

        $access_token = $request->get('access_token');

        $user_entity = $em->getRepository('ApiBundle:User')->findOneBy(array('access_token' => $access_token));

        $credit_cards = array();
        foreach ($user_entity->getCreditCards() as $card) {

            if ($card->getIsRemoved()) continue;

            $orders_count = 0;
            foreach ($card->getOrders() as $order) {
                if ($order->getStatus()->getLabel() === 'Pending') continue;
                $orders_count++;
            }

            $credit_cards[] = array(
                'id'          => $card->getId(),
                'type'        => $card->getType(),
                'lastDigits'  => $card->getLastDigits(),
                'is_bookmark' => $card->getIsBookmark() ? true : false,
                'order_count' => $orders_count,
                'createDate'  => date_format($card->getCreateDate(), 'c'),
            );
        }

        $view = $this->view($credit_cards, 200)
            ->setTemplate(':default:not_available.html.twig')
            ->setTemplateData(array('items' => $credit_cards));
        return $this->handleView($view);
    }
}