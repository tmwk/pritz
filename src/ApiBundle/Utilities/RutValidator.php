<?php

/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 06/05/2016
 * Time: 13:25
 */
namespace ApiBundle\Utilities;

class RutValidator
{
    public static function valida_rut($r)
    {
        if (trim($r) == '') {
            return false;
        }

        $r       = self::removeFormat($r);
        $sub_rut = substr($r, 0, strlen($r) - 1);
        $sub_dv  = substr($r, -1);
        $x       = 2;
        $s       = 0;
        for ($i = strlen($sub_rut) - 1; $i >= 0; $i--) {
            if ($x > 7) {
                $x = 2;
            }
            $s += $sub_rut[$i] * $x;
            $x++;
        }
        $dv = 11 - ($s % 11);
        if ($dv == 10) {
            $dv = 'K';
        }
        if ($dv == 11) {
            $dv = '0';
        }
        if ($dv == $sub_dv) {
            return true;
        } else {
            return false;
        }
    }

    public static function setFormat($rut)
    {
        if ($rut == 0) {
            return '';
        }

        $rut    = self::removeFormat($rut);
        $rutTmp = array();

        if (preg_match('/^([0-9]*)(.)+/', $rut, $rutTmp)) {
            return number_format($rutTmp[1], 0, '', '.') . '-' . $rutTmp[2];
        }
        return '';
    }

    public static function removeFormat($rut)
    {
        return strtoupper(preg_replace('/\.|,|-/', '', $rut));
    }
}