<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 06/05/2016
 * Time: 16:44
 */

namespace ApiBundle\Utilities;


class Helper
{
    public function generateRandom($length, $key)
    {
        $str = '';
        $max = mb_strlen($key, '8bit') - 1;
        if ($max < 1) {
            throw new \Exception($key . 'must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $key[random_int(0, $max)];
        }
        return $str;
    }

    public function randomOnlyNumber($length = 8, $suffix = '')
    {
        return $this->generateRandom($length, '0123456789') . $suffix;
    }

    public function randomStringLowercase($length, $suffix = '')
    {
        return $this->generateRandom($length, 'abcdefghijklmnopqrstuvwxyz') . $suffix;
    }

    public function randomStringUppercase($length, $suffix = '')
    {
        return $this->generateRandom($length, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') . $suffix;
    }

    public function randomPass($length, $suffix = '')
    {
        return $this->generateRandom($length, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') . $suffix;
    }

    public function randomPassLowercase($length, $suffix = '')
    {
        return $this->generateRandom($length, '0123456789abcdefghijklmnopqrstuvwxyz') . $suffix;
    }

    public function randomPassUppercase($length, $suffix = '')
    {
        return $this->generateRandom($length, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ') . $suffix;
    }
}

