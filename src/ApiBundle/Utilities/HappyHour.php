<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 22/09/2016
 * Time: 15:49
 */

namespace ApiBundle\Utilities;

use Doctrine\ORM\EntityManager;

class HappyHour
{
    private static $_em;

    public function __construct(EntityManager $em)
    {
        self::$_em = $em;
    }

    public function check($local)
    {
        $hh = self::$_em->getRepository('ApiBundle:HappyHourTimetable')->findOneBy(array('local' => $local, 'weekday' => date('N')));

        if($hh){
            if (date_format($hh->getStartsAt(), 'H:i:s') < date('H:i:s') && date_format($hh->getEndsAt(), 'H:i:s') > date('H:i:s')) {
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }

    }


}