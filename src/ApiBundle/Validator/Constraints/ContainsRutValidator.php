<?php

/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 23/11/2016
 * Time: 13:16
 */
namespace ApiBundle\Validator\Constraints;

use ApiBundle\Utilities\RutValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class ContainsRutValidator extends ConstraintValidator
{
    protected  $rut;

    public  function  __construct(RutValidator $rut)
    {
        $this->rut = $rut;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$this->rut->valida_rut($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }
    }
}