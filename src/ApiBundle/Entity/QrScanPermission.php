<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 16:37
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="qr_scan_permission")
 */
class QrScanPermission
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $create_date
    *
    * @ORM\Column(type="datetime")
    */
    protected $create_date;

    /**
    * @var string $local
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Local", inversedBy="qr_scan_permissions")
    * @ORM\JoinColumn(name="local_id", referencedColumnName="id")
    */
    private $local;
    
    /**
    * @var string $user
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\User", inversedBy="qr_scan_permissions")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
    private $user;

    /**
     * @var int
     */
    private static $var = 1;

    /**
     * @return int
     */
    public function getNumbers()
    {
        return self::$var++;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return QrScanPermission
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set local
     *
     * @param \ApiBundle\Entity\Local $local
     * @return QrScanPermission
     */
    public function setLocal(\ApiBundle\Entity\Local $local = null)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return \ApiBundle\Entity\Local
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Set user
     *
     * @param \ApiBundle\Entity\User $user
     * @return QrScanPermission
     */
    public function setUser(\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


}
