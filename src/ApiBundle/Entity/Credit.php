<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 17:24
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="credit")
 */
class Credit
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $value
    *
    * @ORM\Column(type="float")
    */
    protected $value;

    /**
    * @var string $create_date
    *
    * @ORM\Column(type="datetime")
    */
    protected $create_date;

    /**
    * @var string $expire_date
    *
    * @ORM\Column(type="datetime")
    */
    protected $expire_date;

    /**
    * @var string $user
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\User", inversedBy="credit")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
    private $user;

    /**
    * @var string $local
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Local", inversedBy="credit")
    * @ORM\JoinColumn(name="local_id", referencedColumnName="id")
    */
    private $local;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return Credit
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return Credit
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set expire_date
     *
     * @param \DateTime $expireDate
     * @return Credit
     */
    public function setExpireDate($expireDate)
    {
        $this->expire_date = $expireDate;

        return $this;
    }

    /**
     * Get expire_date
     *
     * @return \DateTime 
     */
    public function getExpireDate()
    {
        return $this->expire_date;
    }

    /**
     * Set user
     *
     * @param \ApiBundle\Entity\User $user
     * @return Credit
     */
    public function setUser(\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set local
     *
     * @param \ApiBundle\Entity\Local $local
     * @return Credit
     */
    public function setLocal(\ApiBundle\Entity\Local $local = null)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return \ApiBundle\Entity\Local
     */
    public function getLocal()
    {
        return $this->local;
    }
}
