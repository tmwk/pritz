<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 16:51
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_category")
 */
class ProductCategory
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $name
    *
    * @ORM\Column(type="string")
    */
    protected $name;

    /**
    * @var string $create_date
    *
    * @ORM\Column(type="datetime")
    */
    protected $create_date;

    /**
    * @var string $last_update
    *
    * @ORM\Column(type="datetime", nullable=true)
    */
    protected $last_update;

    /**
    * @var string $products
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\Product", mappedBy="category")
    */
    protected $products;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @var int
     */
    private static $var = 1;

    /**
     * @return int
     */
    public function getNumbers()
    {
        return self::$var++;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return ProductCategory
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set last_update
     *
     * @param \DateTime $lastUpdate
     * @return ProductCategory
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get last_update
     *
     * @return \DateTime 
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Add products
     *
     * @param \ApiBundle\Entity\Product $products
     * @return ProductCategory
     */
    public function addProduct(\ApiBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \ApiBundle\Entity\Product $products
     */
    public function removeProduct(\ApiBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }
}
