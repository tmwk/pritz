<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 16:46
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="tax_type")
 */
class TaxType
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $label
    *
    * @ORM\Column(type="string")
    */
    protected $label;

    /**
    * @var string $value
    *
    * @ORM\Column(type="float")
    */
    protected $value;

    /**
    * @var string $products
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\Product", mappedBy="tax_type")
    */
    protected $products;

    public function __toString()
    {
        return $this->getLabel();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return TaxType
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return TaxType
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add products
     *
     * @param \ApiBundle\Entity\Product $products
     * @return TaxType
     */
    public function addProduct(\ApiBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \ApiBundle\Entity\Product $products
     */
    public function removeProduct(\ApiBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }
}
