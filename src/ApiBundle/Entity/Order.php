<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 16:10
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="purchase")
 * @ORM\Entity(repositoryClass="ApiBundle\Entity\OrderRepository")
 */
class Order
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $total
    *
    * @ORM\Column(type="float", nullable=true)
    */
    protected $total;

    /**
    * @var string $status
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\OrderStatus", inversedBy="order")
    * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
    */
    private $status;

    /**
    * @var string $salt
    *
    * @ORM\Column(type="string")
    */
    protected $salt;

    /**
    * @var string $create_date
    *
    * @ORM\Column(type="datetime")
    */
    protected $create_date;

    /**
    * @var string $withdrawn_date
    *
    * @ORM\Column(type="datetime", nullable=true)
    */
    protected $withdrawn_date;

    /**
    * @var string $withdrawn_code
    *
    * @ORM\Column(type="string", nullable=true)
    */
    protected $withdrawn_code;

    /**
    * @var string $local
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Local", inversedBy="orders")
    * @ORM\JoinColumn(name="local_id", referencedColumnName="id")
    */
    private $local;

    /**
    * @var string $user
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\User", inversedBy="orders")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
    private $user;

    /**
    * @var string $credit_card
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\CreditCard", inversedBy="orders")
    * @ORM\JoinColumn(name="credit_card_id", referencedColumnName="id")
    */
    private $credit_card;

    /**
    * @var string $order_item
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\OrderItem", mappedBy="order")
    */
    protected $order_item;


    /**
    * @var string $barman
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\User", inversedBy="orders_received")
    * @ORM\JoinColumn(name="barman_id", referencedColumnName="id")
    */
    private $barman;

    /**
    * @var string $payment
    *
    * @ORM\OneToOne(targetEntity = "ApiBundle\Entity\Payment", mappedBy="order", fetch="EXTRA_LAZY", cascade={"remove"})
    */
    protected $payment;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->order_item = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return Order
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return Order
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return Order
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set local
     *
     * @param \ApiBundle\Entity\Local $local
     * @return Order
     */
    public function setLocal(\ApiBundle\Entity\Local $local = null)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return \ApiBundle\Entity\Local
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Set user
     *
     * @param \ApiBundle\Entity\User $user
     * @return Order
     */
    public function setUser(\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set credit_card
     *
     * @param \ApiBundle\Entity\CreditCard $creditCard
     * @return Order
     */
    public function setCreditCard(\ApiBundle\Entity\CreditCard $creditCard = null)
    {
        $this->credit_card = $creditCard;

        return $this;
    }

    /**
     * Get credit_card
     *
     * @return \ApiBundle\Entity\CreditCard
     */
    public function getCreditCard()
    {
        return $this->credit_card;
    }

    /**
     * Add order_item
     *
     * @param \ApiBundle\Entity\OrderItem $orderItem
     * @return Order
     */
    public function addOrderItem(\ApiBundle\Entity\OrderItem $orderItem)
    {
        $this->order_item[] = $orderItem;

        return $this;
    }

    /**
     * Remove order_item
     *
     * @param \ApiBundle\Entity\OrderItem $orderItem
     */
    public function removeOrderItem(\ApiBundle\Entity\OrderItem $orderItem)
    {
        $this->order_item->removeElement($orderItem);
    }

    /**
     * Get order_item
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderItem()
    {
        return $this->order_item;
    }

    /**
     * Set barman
     *
     * @param \ApiBundle\Entity\User $barman
     * @return Order
     */
    public function setBarman(\ApiBundle\Entity\User $barman = null)
    {
        $this->barman = $barman;

        return $this;
    }

    /**
     * Get barman
     *
     * @return \ApiBundle\Entity\User
     */
    public function getBarman()
    {
        return $this->barman;
    }

    /**
     * Add payment
     *
     * @param \ApiBundle\Entity\Payment $payment
     * @return Order
     */
    public function addPayment(\ApiBundle\Entity\Payment $payment)
    {
        $this->payment[] = $payment;

        return $this;
    }

    /**
     * Remove payment
     *
     * @param \ApiBundle\Entity\Payment $payment
     */
    public function removePayment(\ApiBundle\Entity\Payment $payment)
    {
        $this->payment->removeElement($payment);
    }

    /**
     * Get payment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set payment
     *
     * @param \ApiBundle\Entity\Payment $payment
     * @return Order
     */
    public function setPayment(\ApiBundle\Entity\Payment $payment = null)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Set status
     *
     * @param \ApiBundle\Entity\OrderStatus $status
     * @return Order
     */
    public function setStatus(\ApiBundle\Entity\OrderStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \ApiBundle\Entity\OrderStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set withdrawn_date
     *
     * @param \DateTime $withdrawnDate
     * @return Order
     */
    public function setWithdrawnDate($withdrawnDate)
    {
        $this->withdrawn_date = $withdrawnDate;

        return $this;
    }

    /**
     * Get withdrawn_date
     *
     * @return \DateTime 
     */
    public function getWithdrawnDate()
    {
        return $this->withdrawn_date;
    }

    /**
     * Set withdrawn_code
     *
     * @param string $withdrawnCode
     * @return Order
     */
    public function setWithdrawnCode($withdrawnCode)
    {
        $this->withdrawn_code = $withdrawnCode;

        return $this;
    }

    /**
     * Get withdrawn_code
     *
     * @return string 
     */
    public function getWithdrawnCode()
    {
        return $this->withdrawn_code;
    }
}
