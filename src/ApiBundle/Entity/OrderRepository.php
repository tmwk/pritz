<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 12/05/2016
 * Time: 11:26
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\EntityRepository;

class OrderRepository extends EntityRepository
{
    public function monthlyFromLocal($local)
    {
        $date_from = $this->dates()->from;
        $date_to   = $this->dates()->to;

        return $this->_em
            ->getRepository($this->_entityName)
            ->createQueryBuilder('p')
            ->where('p.local = :id')
            ->andWhere('p.create_date BETWEEN :from AND :to')
            ->setParameters(array(
                'from' => $date_from,
                'to'   => $date_to,
                'id'   => $local,
            ))
            ->getQuery()
            ->getResult();
    }

    public function monthlyFromLocalTotal($local)
    {
        $date_from = $this->dates()->from;
        $date_to   = $this->dates()->to;

        $query = $this->_em
            ->createQuery("SELECT count(p) AS total FROM {$this->_entityName} p WHERE p.local=:id AND p.create_date BETWEEN :from AND :to")
            ->setParameters(array(
                'from' => $date_from,
                'to'   => $date_to,
                'id'   => $local,
            ))
            ->getResult();

        return (int)$query[0]['total'];
    }

    /**
     * Obtiene todas las ordenes de un local y usuario especifico
     * @param $local_id
     * @param $user_id
     * @return array
     */
    public function fromLocalAndUser($local_id, $user_id)
    {
        return $this->_em
            ->getRepository($this->_entityName)
            ->createQueryBuilder('p')
            ->where('p.local = :local_id')
            ->andWhere('p.user = :user_id')
            ->setParameters(array(
                'local_id' => $local_id,
                'user_id'  => $user_id,
            ))
            ->getQuery()
            ->getResult();
    }

    public function fromLocalAndUserTotal($local_id, $user_id)
    {
        $query = $this->_em
            ->createQuery("SELECT count(p) AS total FROM {$this->_entityName} p WHERE p.local = :local_id AND p.user = :user_id")
            ->setParameters(array(
                'local_id' => $local_id,
                'user_id'  => $user_id,
            ))
            ->getResult();
        return (int)$query[0]['total'];
    }

    public function fromUser($user_id)
    {
        return $this->_em
            ->getRepository($this->_entityName)
            ->createQueryBuilder('p')
            ->where('p.user = :user_id')
            ->setParameters(array(
                'user_id' => $user_id,
            ))
            ->getQuery()
            ->getResult();
    }

    private function dates()
    {
        $date      = new \DateTime('now');
        $date_from = new \DateTime($date->format('Y-m-d'));
        $date_from->sub(new \DateInterval('P1M'));

        $date_to = new \DateTime($date->format('Y-m-d'));
        return (object)array(
            'from' => $date_from,
            'to'   => $date_to
        );
    }
}