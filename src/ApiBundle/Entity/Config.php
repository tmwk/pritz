<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 24/11/2016
 * Time: 12:27
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="config")
 */
class Config
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var boolean $app_active
    *
    * @ORM\Column(type="boolean")
    */
    protected $app_active;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set appActive
     *
     * @param boolean $appActive
     *
     * @return Config
     */
    public function setAppActive($appActive)
    {
        $this->app_active = $appActive;

        return $this;
    }

    /**
     * Get appActive
     *
     * @return boolean
     */
    public function getAppActive()
    {
        return $this->app_active;
    }
}
