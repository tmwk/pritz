<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 27/09/2016
 * Time: 17:15
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="payment")
 */
class Payment
{
    const TYPE_WEBPAY_ONECLICK = "WebpayOneClick";
    const TYPE_WEBPAY_NORMAL = "WebpayNormal";

    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $type
    *
    * @ORM\Column(type="string", columnDefinition="enum('WebpayOneClick', 'WebpayNormal')")
    */
    protected $type;

    /**
    * @var string $authorization_code
    *
    * @ORM\Column(type="integer")
    */
    protected $authorization_code;

    /**
    * @var string $credit_card_type
    *
    * @ORM\Column(type="string", nullable=true)
    */
    protected $credit_card_type;

    /**
    * @var string $last_card_digits
    *
    * @ORM\Column(type="integer")
    */
    protected $last_card_digits;

    /**
    * @var string $response_code
    *
    * @ORM\Column(type="string")
    */
    protected $response_code;

    /**
    * @var string $transaction_id
    *
    * @ORM\Column(type="integer", nullable=true)
    */
    protected $transaction_id;

    /**
    * @var string $order
    *
    * @ORM\OneToOne(targetEntity="ApiBundle\Entity\Order", inversedBy="payment")
    * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
    */
    private $order;

    /**
    * @var string $accounting_date
    *
    * @ORM\Column(type="string", length=4, nullable=true)
    */
    protected $accounting_date;
    
    /**
    * @var string $payment_type_code
    *
    * @ORM\Column(type="string", length=2, nullable=true)
    */
    protected $payment_type_code;

    /**
    * @var string $shares_number
    *
    * @ORM\Column(type="integer", length=4, nullable=true)
    */
    protected $shares_number;

    /**
    * @var string $vci
    *
    * @ORM\Column(type="string", length=3, nullable=true)
    */
    protected $vci;

    /**
    * @var string $token
    *
    * @ORM\Column(type="string", length=64, nullable=true)
    */
    protected $token;

    /**
     * @var string $create_date
     *
     * @ORM\Column(type="datetime")
     */
    protected $create_date;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set authorization_code
     *
     * @param integer $authorizationCode
     * @return Payment
     */
    public function setAuthorizationCode($authorizationCode)
    {
        $this->authorization_code = $authorizationCode;

        return $this;
    }

    /**
     * Get authorization_code
     *
     * @return integer 
     */
    public function getAuthorizationCode()
    {
        return $this->authorization_code;
    }

    /**
     * Set credit_card_type
     *
     * @param string $creditCardType
     * @return Payment
     */
    public function setCreditCardType($creditCardType)
    {
        $this->credit_card_type = $creditCardType;

        return $this;
    }

    /**
     * Get credit_card_type
     *
     * @return string 
     */
    public function getCreditCardType()
    {
        return $this->credit_card_type;
    }

    /**
     * Set last_card_digits
     *
     * @param integer $lastCardDigits
     * @return Payment
     */
    public function setLastCardDigits($lastCardDigits)
    {
        $this->last_card_digits = $lastCardDigits;

        return $this;
    }

    /**
     * Get last_card_digits
     *
     * @return integer 
     */
    public function getLastCardDigits()
    {
        return $this->last_card_digits;
    }

    /**
     * Set response_code
     *
     * @param string $responseCode
     * @return Payment
     */
    public function setResponseCode($responseCode)
    {
        $this->response_code = $responseCode;

        return $this;
    }

    /**
     * Get response_code
     *
     * @return string 
     */
    public function getResponseCode()
    {
        return $this->response_code;
    }

    /**
     * Set transaction_id
     *
     * @param integer $transactionId
     * @return Payment
     */
    public function setTransactionId($transactionId)
    {
        $this->transaction_id = $transactionId;

        return $this;
    }

    /**
     * Get transaction_id
     *
     * @return integer 
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    /**
     * Set order
     *
     * @param \ApiBundle\Entity\Order $order
     * @return Payment
     */
    public function setOrder(\ApiBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \ApiBundle\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return Payment
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Payment
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set accounting_date
     *
     * @param string $accountingDate
     * @return Payment
     */
    public function setAccountingDate($accountingDate)
    {
        $this->accounting_date = $accountingDate;

        return $this;
    }

    /**
     * Get accounting_date
     *
     * @return string 
     */
    public function getAccountingDate()
    {
        return $this->accounting_date;
    }

    /**
     * Set payment_type_code
     *
     * @param string $paymentTypeCode
     * @return Payment
     */
    public function setPaymentTypeCode($paymentTypeCode)
    {
        $this->payment_type_code = $paymentTypeCode;

        return $this;
    }

    /**
     * Get payment_type_code
     *
     * @return string 
     */
    public function getPaymentTypeCode()
    {
        return $this->payment_type_code;
    }

    /**
     * Set shares_number
     *
     * @param integer $sharesNumber
     * @return Payment
     */
    public function setSharesNumber($sharesNumber)
    {
        $this->shares_number = $sharesNumber;

        return $this;
    }

    /**
     * Get shares_number
     *
     * @return integer 
     */
    public function getSharesNumber()
    {
        return $this->shares_number;
    }

    /**
     * Set vci
     *
     * @param string $vci
     * @return Payment
     */
    public function setVci($vci)
    {
        $this->vci = $vci;

        return $this;
    }

    /**
     * Get vci
     *
     * @return string 
     */
    public function getVci()
    {
        return $this->vci;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Payment
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }
}
