<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 16:27
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use ApiBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @DoctrineAssert\UniqueEntity(fields="email", message="El email ya existe")
 * @DoctrineAssert\UniqueEntity(fields="tw_uid", message="El Twitter ID ya existe")
 * @DoctrineAssert\UniqueEntity(fields="fb_uid", message="El Facebook ID ya existe")
 *
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $fb_uid
     *
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    protected $fb_uid;

    /**
     * @var string $tw_uid
     *
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    protected $tw_uid;

    /**
     * @var string $firstname
     *
     * @Assert\NotBlank(message="Ingresa el nombre")
     *
     * @ORM\Column(type="string")
     */
    protected $firstname;

    /**
     * @var string $lastname
     *
     * @Assert\NotBlank(message="Ingresa los apellidos")
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $lastname;

    /**
     * @var string $birthdate
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $birthdate;

    /**
     * @var string $rut
     *
     * @Assert\NotBlank(message="Ingresa el rut")
     * @CustomAssert\ContainsRut()
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $rut;

    /**
     * @var string $email
     * 
     * @Assert\NotBlank(message="Ingresa el E-mail")
     * @Assert\Email(message = "El email {{ value }} no es valido.", checkMX = true)
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $email;

    /**
     * @var string $password
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $password;

    /**
     * @var string $access_token
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $access_token;

    /**
     * @var string $secret_code
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $secret_code;

    /**
     * @var string $register_date
     *
     * @ORM\Column(type="datetime")
     */
    protected $register_date;

    /**
     * @var string $last_update
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $last_update;

    /**
     * @var string $last_login
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $last_login;

    /**
     * @var string $orders
     *
     * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\Order", mappedBy="user", fetch="EXTRA_LAZY", cascade={"remove"})
     * @ORM\OrderBy({"id" = "DESC"})
     *
     */
    protected $orders;

    /**
     * @var string $credit_cards
     *
     * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\CreditCard", mappedBy="user", fetch="EXTRA_LAZY", cascade={"remove"})
     */
    protected $credit_cards;

    /**
     * @var string $qr_scan_permissions
     *
     * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\QrScanPermission", mappedBy="user")
     */
    protected $qr_scan_permissions;

    /**
     * @var string $product_bookmark
     *
     * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\ProductBookmark", mappedBy="user")
     */
    protected $product_bookmark;

    /**
     * @var string $credit
     *
     * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\Credit", mappedBy="user")
     */
    protected $credit;

    /**
     * @var string $orders_received
     *
     * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\Order", mappedBy="barman")
     * @ORM\OrderBy({"withdrawn_date" = "DESC"})
     */
    protected $orders_received;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders              = new \Doctrine\Common\Collections\ArrayCollection();
        $this->credit_cards        = new \Doctrine\Common\Collections\ArrayCollection();
        $this->qr_scan_permissions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->product_bookmark    = new \Doctrine\Common\Collections\ArrayCollection();
        $this->credit              = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getFullName()
    {
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     * @return User
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set rut
     *
     * @param string $rut
     * @return User
     */
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * Get rut
     *
     * @return string
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = md5($password);

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set register_date
     *
     * @param \DateTime $registerDate
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->register_date = $registerDate;

        return $this;
    }

    /**
     * Get register_date
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->register_date;
    }

    /**
     * Set last_update
     *
     * @param \DateTime $lastUpdate
     * @return User
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get last_update
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set last_login
     *
     * @param \DateTime $lastLogin
     * @return User
     */
    public function setLastLogin($lastLogin)
    {
        $this->last_login = $lastLogin;

        return $this;
    }

    /**
     * Get last_login
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->last_login;
    }

    /**
     * Add orders
     *
     * @param \ApiBundle\Entity\Order $orders
     * @return User
     */
    public function addOrder(\ApiBundle\Entity\Order $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \ApiBundle\Entity\Order $orders
     */
    public function removeOrder(\ApiBundle\Entity\Order $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add credit_cards
     *
     * @param \ApiBundle\Entity\CreditCard $creditCards
     * @return User
     */
    public function addCreditCard(\ApiBundle\Entity\CreditCard $creditCards)
    {
        $this->credit_cards[] = $creditCards;

        return $this;
    }

    /**
     * Remove credit_cards
     *
     * @param \ApiBundle\Entity\CreditCard $creditCards
     */
    public function removeCreditCard(\ApiBundle\Entity\CreditCard $creditCards)
    {
        $this->credit_cards->removeElement($creditCards);
    }

    /**
     * Get credit_cards
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCreditCards()
    {
        return $this->credit_cards;
    }

    /**
     * Add qr_scan_permissions
     *
     * @param \ApiBundle\Entity\QrScanPermission $qrScanPermissions
     * @return User
     */
    public function addQrScanPermission(\ApiBundle\Entity\QrScanPermission $qrScanPermissions)
    {
        $this->qr_scan_permissions[] = $qrScanPermissions;

        return $this;
    }

    /**
     * Remove qr_scan_permissions
     *
     * @param \ApiBundle\Entity\QrScanPermission $qrScanPermissions
     */
    public function removeQrScanPermission(\ApiBundle\Entity\QrScanPermission $qrScanPermissions)
    {
        $this->qr_scan_permissions->removeElement($qrScanPermissions);
    }

    /**
     * Get qr_scan_permissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQrScanPermissions()
    {
        return $this->qr_scan_permissions;
    }

    /**
     * Add product_bookmark
     *
     * @param \ApiBundle\Entity\ProductBookmark $productBookmark
     * @return User
     */
    public function addProductBookmark(\ApiBundle\Entity\ProductBookmark $productBookmark)
    {
        $this->product_bookmark[] = $productBookmark;

        return $this;
    }

    /**
     * Remove product_bookmark
     *
     * @param \ApiBundle\Entity\ProductBookmark $productBookmark
     */
    public function removeProductBookmark(\ApiBundle\Entity\ProductBookmark $productBookmark)
    {
        $this->product_bookmark->removeElement($productBookmark);
    }

    /**
     * Get product_bookmark
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductBookmark()
    {
        return $this->product_bookmark;
    }

    /**
     * Add credit
     *
     * @param \ApiBundle\Entity\Credit $credit
     * @return User
     */
    public function addCredit(\ApiBundle\Entity\Credit $credit)
    {
        $this->credit[] = $credit;

        return $this;
    }

    /**
     * Remove credit
     *
     * @param \ApiBundle\Entity\Credit $credit
     */
    public function removeCredit(\ApiBundle\Entity\Credit $credit)
    {
        $this->credit->removeElement($credit);
    }

    /**
     * Get credit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Add orders_received
     *
     * @param \ApiBundle\Entity\Order $ordersReceived
     * @return User
     */
    public function addOrdersReceived(\ApiBundle\Entity\Order $ordersReceived)
    {
        $this->orders_received[] = $ordersReceived;

        return $this;
    }

    /**
     * Remove orders_received
     *
     * @param \ApiBundle\Entity\Order $ordersReceived
     */
    public function removeOrdersReceived(\ApiBundle\Entity\Order $ordersReceived)
    {
        $this->orders_received->removeElement($ordersReceived);
    }

    /**
     * Get orders_received
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrdersReceived()
    {
        return $this->orders_received;
    }

    /**
     * Set access_token
     *
     * @param string $accessToken
     * @return User
     */
    public function setAccessToken($accessToken)
    {
        $this->access_token = $accessToken;

        return $this;
    }

    /**
     * Get access_token
     *
     * @return string
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }

    /**
     * Set secret_code
     *
     * @param string $secretCode
     * @return User
     */
    public function setSecretCode($secretCode)
    {
        $this->secret_code = $secretCode;

        return $this;
    }

    /**
     * Get secret_code
     *
     * @return string
     */
    public function getSecretCode()
    {
        return $this->secret_code;
    }

    /**
     * Set fb_uid
     *
     * @param string $fbUid
     * @return User
     */
    public function setFbUid($fbUid)
    {
        $this->fb_uid = $fbUid;

        return $this;
    }

    /**
     * Get fb_uid
     *
     * @return string
     */
    public function getFbUid()
    {
        return $this->fb_uid;
    }

    /**
     * Set tw_uid
     *
     * @param string $twUid
     * @return User
     */
    public function setTwUid($twUid)
    {
        $this->tw_uid = $twUid;

        return $this;
    }

    /**
     * Get tw_uid
     *
     * @return string
     */
    public function getTwUid()
    {
        return $this->tw_uid;
    }
}
