<?php

/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 15:34
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="local")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="ApiBundle\Entity\LocalRepository")
 */
class Local
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $name
    *
    * @ORM\Column(type="string")
    */
    protected $name;

    /**
    * @var string $status_text
    *
    * @ORM\Column(type="string", nullable=true, options={"collate"="utf8mb4_general_ci"})
    */
    protected $status_text;

    /**
    * @var string $image_filename
    *
    * @ORM\Column(type="string", nullable=true)
    */
    protected $image_filename;

    /**
    * @Assert\Image(
     *   maxSize = "1024k",
     *   maxSizeMessage = "El archivo es demasiado grande ({{ size }}), el tama�o permitido es {{ limit }}",
     *   mimeTypes = {"image/png", "image/jpeg", "image/gif"},
     *   mimeTypesMessage = "El tipo mime del archivo no es v�lido ({{type}}). Tipos MIME permitidos son {{types}}",
     *   notFoundMessage = "Archivo no encontrado",
     *   notReadableMessage = "No tiene permisos de escritura",
     *   uploadIniSizeErrorMessage = "El servidor solo permite archivos de tama�o inferior a {{ limit }}",
     *   uploadFormSizeErrorMessage = "El servidor solo permite archivos de tama�o inferior a {{ limit }} a traves de formularios",
     *   uploadErrorMessage = "Error al subir el archivo"
     * )
    **/
    protected $file;

    /**
    * @var string $enabled
    *
    * @ORM\Column(type="boolean")
    */
    protected $enabled;

    /**
    * @var string $latitude
    *
    * @ORM\Column(type="float")
    */
    protected $latitude;

    /**
    * @var string $longitude
    *
    * @ORM\Column(type="float")
    */
    protected $longitude;

    /**
    * @var string $order_timeout
    *
    * @ORM\Column(type="integer")
    */
    protected $order_timeout;

    /**
    * @var string $credit_timeout
    *
    * @ORM\Column(type="integer")
    */
    protected $credit_timeout;

    /**
    * @var string $happy_hour_enabled
    *
    * @ORM\Column(type="boolean")
    */
    protected $happy_hour_enabled;

    /**
    * @var string $create_date
    *
    * @ORM\Column(type="datetime")
    */
    protected $create_date;

    /**
    * @var string $last_update
    *
    * @ORM\Column(type="datetime", nullable=true)
    */
    protected $last_update;

    /**
    * @var string $client
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Client", inversedBy="local")
    * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
    */
    private $client;

    /**
    * @var string $event_type
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\EventType", inversedBy="local")
    * @ORM\JoinColumn(name="event_type_id", referencedColumnName="id")
    */
    private $event_type;
    
    /**
    * @var string $orders
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\Order", mappedBy="local", fetch="EXTRA_LAZY", cascade={"remove"})
    */
    protected $orders;

    /**
    * @var string $qr_scan_permissions
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\QrScanPermission", mappedBy="local")
    */
    protected $qr_scan_permissions;

    /**
    * @var string $products
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\Product", mappedBy="local")
    */
    protected $products;

    /**
    * @var string $credit
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\Credit", mappedBy="local")
    */
    protected $credit;

    /**
    * @var string $happy_hour_timetable
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\HappyHourTimetable", mappedBy="local")
    */
    protected $happy_hour_timetable;

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getWebPathImage()
    {
        return $this->getWebFullPath().$this->getImageFilename();
    }

    /**
     * @return string
     */
    public function getUploadDir()
    {
        return __DIR__ . '/../../../web'.$this->getWebFullPath();
    }

    /**
     * @return string
     */
    public function getWebFullPath()
    {
        return '/images/local/';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $this->removeFile();

        $file = $this->file;

        $fileName = md5(uniqid()) . '.' . $file->guessExtension();

        // Move the file to the directory where brochures are stored
        $file->move($this->getUploadDir(), $fileName);

        $this->image_filename = $fileName;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeFile()
    {
        if (file_exists($this->getUploadDir() . $this->getImageFilename())) {
            @unlink($this->getUploadDir() . $this->getImageFilename());
        }
    }

    /**
     * @var int
     */
    private static $var = 1;

    /**
     * @return int
     */
    public function getNumbers()
    {
        return self::$var++;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new \Doctrine\Common\Collections\ArrayCollection();
        $this->qr_scan_permissions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
        $this->credit = new \Doctrine\Common\Collections\ArrayCollection();
        $this->happy_hour_timetable = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Local
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status_text
     *
     * @param string $statusText
     * @return Local
     */
    public function setStatusText($statusText)
    {
        $this->status_text = $statusText;

        return $this;
    }

    /**
     * Get status_text
     *
     * @return string 
     */
    public function getStatusText()
    {
        return $this->status_text;
    }

    /**
     * Set image_filename
     *
     * @param string $imageFilename
     * @return Local
     */
    public function setImageFilename($imageFilename)
    {
        $this->image_filename = $imageFilename;

        return $this;
    }

    /**
     * Get image_filename
     *
     * @return string 
     */
    public function getImageFilename()
    {
        return $this->image_filename;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Local
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Local
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Local
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set order_timeout
     *
     * @param integer $orderTimeout
     * @return Local
     */
    public function setOrderTimeout($orderTimeout)
    {
        $this->order_timeout = $orderTimeout;

        return $this;
    }

    /**
     * Get order_timeout
     *
     * @return integer 
     */
    public function getOrderTimeout()
    {
        return $this->order_timeout;
    }

    /**
     * Set credit_timeout
     *
     * @param integer $creditTimeout
     * @return Local
     */
    public function setCreditTimeout($creditTimeout)
    {
        $this->credit_timeout = $creditTimeout;

        return $this;
    }

    /**
     * Get credit_timeout
     *
     * @return integer 
     */
    public function getCreditTimeout()
    {
        return $this->credit_timeout;
    }

    /**
     * Set happy_hour_enabled
     *
     * @param boolean $happyHourEnabled
     * @return Local
     */
    public function setHappyHourEnabled($happyHourEnabled)
    {
        $this->happy_hour_enabled = $happyHourEnabled;

        return $this;
    }

    /**
     * Get happy_hour_enabled
     *
     * @return boolean 
     */
    public function getHappyHourEnabled()
    {
        return $this->happy_hour_enabled;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return Local
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set last_update
     *
     * @param \DateTime $lastUpdate
     * @return Local
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get last_update
     *
     * @return \DateTime 
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set client
     *
     * @param \ApiBundle\Entity\Client $client
     * @return Local
     */
    public function setClient(\ApiBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \ApiBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set event_type
     *
     * @param \ApiBundle\Entity\EventType $eventType
     * @return Local
     */
    public function setEventType(\ApiBundle\Entity\EventType $eventType = null)
    {
        $this->event_type = $eventType;

        return $this;
    }

    /**
     * Get event_type
     *
     * @return \ApiBundle\Entity\EventType
     */
    public function getEventType()
    {
        return $this->event_type;
    }

    /**
     * Add orders
     *
     * @param \ApiBundle\Entity\Order $orders
     * @return Local
     */
    public function addOrder(\ApiBundle\Entity\Order $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \ApiBundle\Entity\Order $orders
     */
    public function removeOrder(\ApiBundle\Entity\Order $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add qr_scan_permissions
     *
     * @param \ApiBundle\Entity\QrScanPermission $qrScanPermissions
     * @return Local
     */
    public function addQrScanPermission(\ApiBundle\Entity\QrScanPermission $qrScanPermissions)
    {
        $this->qr_scan_permissions[] = $qrScanPermissions;

        return $this;
    }

    /**
     * Remove qr_scan_permissions
     *
     * @param \ApiBundle\Entity\QrScanPermission $qrScanPermissions
     */
    public function removeQrScanPermission(\ApiBundle\Entity\QrScanPermission $qrScanPermissions)
    {
        $this->qr_scan_permissions->removeElement($qrScanPermissions);
    }

    /**
     * Get qr_scan_permissions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getQrScanPermissions()
    {
        return $this->qr_scan_permissions;
    }

    /**
     * Add products
     *
     * @param \ApiBundle\Entity\Product $products
     * @return Local
     */
    public function addProduct(\ApiBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \ApiBundle\Entity\Product $products
     */
    public function removeProduct(\ApiBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add credit
     *
     * @param \ApiBundle\Entity\Credit $credit
     * @return Local
     */
    public function addCredit(\ApiBundle\Entity\Credit $credit)
    {
        $this->credit[] = $credit;

        return $this;
    }

    /**
     * Remove credit
     *
     * @param \ApiBundle\Entity\Credit $credit
     */
    public function removeCredit(\ApiBundle\Entity\Credit $credit)
    {
        $this->credit->removeElement($credit);
    }

    /**
     * Get credit
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Add happy_hour_timetable
     *
     * @param \ApiBundle\Entity\HappyHourTimetable $happyHourTimetable
     * @return Local
     */
    public function addHappyHourTimetable(\ApiBundle\Entity\HappyHourTimetable $happyHourTimetable)
    {
        $this->happy_hour_timetable[] = $happyHourTimetable;

        return $this;
    }

    /**
     * Remove happy_hour_timetable
     *
     * @param \ApiBundle\Entity\HappyHourTimetable $happyHourTimetable
     */
    public function removeHappyHourTimetable(\ApiBundle\Entity\HappyHourTimetable $happyHourTimetable)
    {
        $this->happy_hour_timetable->removeElement($happyHourTimetable);
    }

    /**
     * Get happy_hour_timetable
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHappyHourTimetable()
    {
        return $this->happy_hour_timetable;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Local
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }
}
