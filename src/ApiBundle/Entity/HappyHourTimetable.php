<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 17:51
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="happy_hour_timetable")
 * @ORM\HasLifecycleCallbacks()
 */
class HappyHourTimetable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $weekday
     *
     * @Assert\NotBlank(message="Seleccione un día")
     *
     * @ORM\Column(type="integer")
     */
    protected $weekday;

    /**
     * @var string $starts_at
     *
     * @Assert\Time(message="La hora no es valida.")
     *
     * @ORM\Column(type="time")
     */
    protected $starts_at;

    /**
     * @var string $ends_at
     *
     * @Assert\Time(message="La hora no es valida.")
     *
     * @ORM\Column(type="time")
     */
    protected $ends_at;

    /**
     * @var string $create_date
     *
     * @ORM\Column(type="datetime")
     */
    protected $create_date;

    /**
     * @var string $last_update
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $last_update;

    /**
     * @var string $local
     *
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Local", inversedBy="happy_hour_timetable")
     * @ORM\JoinColumn(name="local_id", referencedColumnName="id")
     */
    private $local;

    public function __toString()
    {
        return $this->getWeekdayString() . ' de ' . date_format($this->getStartsAt(), 'H:i') . ' a ' . date_format($this->getEndsAt(), 'H:i');
    }

    private static $var = 1;

    public function getNumbers()
    {
        return self::$var++;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     */
    public function lastUpdate()
    {
        $this->setLastUpdate(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set weekday
     *
     * @param integer $weekday
     *
     * @return HappyHourTimetable
     */
    public function setWeekday($weekday)
    {
        $this->weekday = $weekday;

        return $this;
    }

    /**
     * Get weekday
     *
     * @return integer
     */
    public function getWeekday()
    {
        return $this->weekday;
    }

    /**
     * Get weekday
     *
     * @return integer
     */
    public function getWeekdayString()
    {
        switch ($this->weekday){
            case 1: return 'Lunes'; break;
            case 2: return 'Martes'; break;
            case 3: return 'Miercoles'; break;
            case 4: return 'Jueves'; break;
            case 5: return 'Viernes'; break;
            case 6: return 'Sabado'; break;
            case 7: return 'Domingo'; break;
        }
        return $this->weekday;
    }

    /**
     * Set startsAt
     *
     * @param \DateTime $startsAt
     *
     * @return HappyHourTimetable
     */
    public function setStartsAt($startsAt)
    {
        $this->starts_at = $startsAt;

        return $this;
    }

    /**
     * Get startsAt
     *
     * @return \DateTime
     */
    public function getStartsAt()
    {
        return $this->starts_at;
    }

    /**
     * Set endsAt
     *
     * @param \DateTime $endsAt
     *
     * @return HappyHourTimetable
     */
    public function setEndsAt($endsAt)
    {
        $this->ends_at = $endsAt;

        return $this;
    }

    /**
     * Get endsAt
     *
     * @return \DateTime
     */
    public function getEndsAt()
    {
        return $this->ends_at;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return HappyHourTimetable
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return HappyHourTimetable
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set local
     *
     * @param \ApiBundle\Entity\Local $local
     *
     * @return HappyHourTimetable
     */
    public function setLocal(\ApiBundle\Entity\Local $local = null)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return \ApiBundle\Entity\Local
     */
    public function getLocal()
    {
        return $this->local;
    }
}
