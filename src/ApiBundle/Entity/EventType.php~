<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 15:52
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="event_type")
 */
class EventType
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $icon_filename
    *
    * @ORM\Column(type="string")
    */
    protected $icon_filename;

    /**
    * @var string $label
    *
    * @ORM\Column(type="string")
    */
    protected $label;

    /**
    * @var string $local
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\Local", mappedBy="event_type")
    */
    protected $local;

    public function __toString()
    {
        return $this->getLabel();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->local = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set icon_filename
     *
     * @param string $iconFilename
     * @return EventType
     */
    public function setIconFilename($iconFilename)
    {
        $this->icon_filename = $iconFilename;

        return $this;
    }

    /**
     * Get icon_filename
     *
     * @return string 
     */
    public function getIconFilename()
    {
        return $this->icon_filename;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return EventType
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Add local
     *
     * @param \ApiBundle\Entity\Local $local
     * @return EventType
     */
    public function addLocal(\ApiBundle\Entity\Local $local)
    {
        $this->local[] = $local;

        return $this;
    }

    /**
     * Remove local
     *
     * @param \ApiBundle\Entity\Local $local
     */
    public function removeLocal(\ApiBundle\Entity\Local $local)
    {
        $this->local->removeElement($local);
    }

    /**
     * Get local
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocal()
    {
        return $this->local;
    }
}
