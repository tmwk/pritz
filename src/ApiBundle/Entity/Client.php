<?php

/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 15:14
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use ApiBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="client")
 * @DoctrineAssert\UniqueEntity(fields="rut", message="El Rut ya existe")
 * @DoctrineAssert\UniqueEntity(fields="email", message="El Email ya existe")
 */
class Client implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $business_name
     *
     * @ORM\Column(type="string")
     */
    protected $business_name;

    /**
     * @var string $fantasy_name
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $fantasy_name;

    /**
     * @var string $rut
     *
     * @ORM\Column(type="string")
     * @CustomAssert\ContainsRut()
     */
    protected $rut;

    /**
     * @var string $email
     *
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @var string $address
     *
     * @ORM\Column(type="string")
     */
    protected $address;

    /**
     * @var string $city
     *
     * @ORM\Column(type="string")
     */
    protected $city;

    /**
     * @var string $phone_number
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone_number;

    /**
     * @var string $create_date
     *
     * @ORM\Column(type="datetime")
     */
    protected $create_date;

    /**
     * @var string $last_update
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $last_update;

    /**
     * @var string $local
     *
     * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\Local", mappedBy="client", fetch="EXTRA_LAZY", cascade={"remove"})
     */
    protected $local;

    /**
     * @var string $salt
     *
     * @ORM\Column(type="string")
     */
    protected $salt;

    /**
     * @var string $password
     *
     * @ORM\Column(type="string")
     */
    protected $password;

    public function __toString()
    {
        return $this->getBusinessName();
    }

    private static $var = 1;

    public function getNumbers()
    {
        return self::$var++;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     */
    public function getRoles()
    {
        return array('ROLE_CLIENT');
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->business_name;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->local = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set business_name
     *
     * @param string $businessName
     * @return Client
     */
    public function setBusinessName($businessName)
    {
        $this->business_name = $businessName;

        return $this;
    }

    /**
     * Get business_name
     *
     * @return string
     */
    public function getBusinessName()
    {
        return $this->business_name;
    }

    /**
     * Set fantasy_name
     *
     * @param string $fantasyName
     * @return Client
     */
    public function setFantasyName($fantasyName)
    {
        $this->fantasy_name = $fantasyName;

        return $this;
    }

    /**
     * Get fantasy_name
     *
     * @return string
     */
    public function getFantasyName()
    {
        return $this->fantasy_name;
    }

    /**
     * Set rut
     *
     * @param string $rut
     * @return Client
     */
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * Get rut
     *
     * @return string
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Client
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Client
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set phone_number
     *
     * @param string $phoneNumber
     * @return Client
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phone_number = $phoneNumber;

        return $this;
    }

    /**
     * Get phone_number
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return Client
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set last_update
     *
     * @param \DateTime $lastUpdate
     * @return Client
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get last_update
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Add local
     *
     * @param \ApiBundle\Entity\Client $local
     * @return Client
     */
    public function addLocal(\ApiBundle\Entity\Client $local)
    {
        $this->local[] = $local;

        return $this;
    }

    /**
     * Remove local
     *
     * @param \ApiBundle\Entity\Client $local
     */
    public function removeLocal(\ApiBundle\Entity\Client $local)
    {
        $this->local->removeElement($local);
    }

    /**
     * Get local
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return Client
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Client
     */
    public function setPassword($password)
    {
        if (!empty($password)) {
            $this->salt = md5(time());
            $encoder = new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('sha512', true, 1);
            $password2 = $encoder->encodePassword($password, $this->getSalt());
            $this->password = $password2;
        }

        return $this;
    }
}
