<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 16:45
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 */
class Product
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $name
     *
     * @Assert\NotBlank(message="Ingrese el nombre del producto")
    *
    * @ORM\Column(type="string")
    */
    protected $name;

    /**
    * @var string $description
     *
     * @Assert\NotBlank(message="Ingrese la descripción del producto")
    *
    * @ORM\Column(type="text")
    */
    protected $description;

    /**
    * @var string $price
     *
     * @Assert\NotBlank(message="Ingrese el valor del producto")
    *
    * @ORM\Column(type="float")
    */
    protected $price;
    
    /**
    * @var string $price_hh
    *
    * @ORM\Column(type="float", nullable=true)
    */
    protected $price_hh;

    /**
    * @var string $create_date
    *
    * @ORM\Column(type="datetime")
    */
    protected $create_date;

    /**
    * @var string $last_update
    *
    * @ORM\Column(type="datetime", nullable=true)
    */
    protected $last_update;

    /**
    * @var string $category
     *
     * @Assert\NotBlank(message="Seleccione una categoria")
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\ProductCategory", inversedBy="products")
    * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
    */
    private $category;

    /**
    * @var string $tax_type
     * 
     * @Assert\NotBlank(message="Seleccione un valor de impuesto")
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\TaxType", inversedBy="products")
    * @ORM\JoinColumn(name="tax_type_id", referencedColumnName="id")
    */
    private $tax_type;

    /**
    * @var string $local
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Local", inversedBy="products")
    * @ORM\JoinColumn(name="local_id", referencedColumnName="id")
    */
    private $local;

    /**
    * @var string $product_bookmark
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\ProductBookmark", mappedBy="product")
    */
    protected $product_bookmark;

    /**
    * @var string $order_item
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\OrderItem", mappedBy="product")
    */
    protected $order_item;

    /**
     * @var int
     */
    private static $var = 1;

    /**
     * @return int
     */
    public function getNumbers()
    {
        return self::$var++;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->product_bookmark = new \Doctrine\Common\Collections\ArrayCollection();
        $this->order_item = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return Product
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set last_update
     *
     * @param \DateTime $lastUpdate
     * @return Product
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get last_update
     *
     * @return \DateTime 
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set category
     *
     * @param \ApiBundle\Entity\ProductCategory $category
     * @return Product
     */
    public function setCategory(\ApiBundle\Entity\ProductCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \ApiBundle\Entity\ProductCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set tax_type
     *
     * @param \ApiBundle\Entity\TaxType $taxType
     * @return Product
     */
    public function setTaxType(\ApiBundle\Entity\TaxType $taxType = null)
    {
        $this->tax_type = $taxType;

        return $this;
    }

    /**
     * Get tax_type
     *
     * @return \ApiBundle\Entity\TaxType
     */
    public function getTaxType()
    {
        return $this->tax_type;
    }

    /**
     * Set local
     *
     * @param \ApiBundle\Entity\Local $local
     * @return Product
     */
    public function setLocal(\ApiBundle\Entity\Local $local = null)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return \ApiBundle\Entity\Local
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Add product_bookmark
     *
     * @param \ApiBundle\Entity\ProductBookmark $productBookmark
     * @return Product
     */
    public function addProductBookmark(\ApiBundle\Entity\ProductBookmark $productBookmark)
    {
        $this->product_bookmark[] = $productBookmark;

        return $this;
    }

    /**
     * Remove product_bookmark
     *
     * @param \ApiBundle\Entity\ProductBookmark $productBookmark
     */
    public function removeProductBookmark(\ApiBundle\Entity\ProductBookmark $productBookmark)
    {
        $this->product_bookmark->removeElement($productBookmark);
    }

    /**
     * Get product_bookmark
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductBookmark()
    {
        return $this->product_bookmark;
    }

    /**
     * Add order_item
     *
     * @param \ApiBundle\Entity\OrderItem $orderItem
     * @return Product
     */
    public function addOrderItem(\ApiBundle\Entity\OrderItem $orderItem)
    {
        $this->order_item[] = $orderItem;

        return $this;
    }

    /**
     * Remove order_item
     *
     * @param \ApiBundle\Entity\OrderItem $orderItem
     */
    public function removeOrderItem(\ApiBundle\Entity\OrderItem $orderItem)
    {
        $this->order_item->removeElement($orderItem);
    }

    /**
     * Get order_item
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderItem()
    {
        return $this->order_item;
    }

    /**
     * Set price_hh
     *
     * @param float $priceHh
     * @return Product
     */
    public function setPriceHh($priceHh)
    {
        $this->price_hh = $priceHh;

        return $this;
    }

    /**
     * Get price_hh
     *
     * @return float 
     */
    public function getPriceHh()
    {
        return $this->price_hh;
    }
}
