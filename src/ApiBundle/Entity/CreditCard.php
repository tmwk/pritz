<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 05/05/2016
 * Time: 16:32
 */

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="credit_card")
 */
class CreditCard
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var string $type
    *
    * @ORM\Column(type="string", nullable=true)
    */
    protected $type;

    /**
    * @var string $last_digits
    *
    * @ORM\Column(type="integer", nullable=true)
    */
    protected $last_digits;

    /**
    * @var string $token
    *
    * @ORM\Column(type="string", nullable=true)
    */
    protected $token;

    /**
    * @var string $auth_code
    *
    * @ORM\Column(type="integer")
    */
    protected $auth_code;

    /**
    * @var string $is_bookmark
    *
    * @ORM\Column(type="boolean", nullable=true)
    */
    protected $is_bookmark;

    /**
     * @var string $is_removed
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $is_removed;

    /**
    * @var string $create_date
    *
    * @ORM\Column(type="datetime")
    */
    protected $create_date;
    
    /**
    * @var string $user
    *
    * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\User", inversedBy="credit_cards")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
    private $user;

    /**
    * @var string $orders
    *
    * @ORM\OneToMany(targetEntity = "ApiBundle\Entity\Order", mappedBy="credit_card")
    */
    protected $orders;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return CreditCard
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set last_digits
     *
     * @param integer $lastDigits
     * @return CreditCard
     */
    public function setLastDigits($lastDigits)
    {
        $this->last_digits = $lastDigits;

        return $this;
    }

    /**
     * Get last_digits
     *
     * @return integer 
     */
    public function getLastDigits()
    {
        return $this->last_digits;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return CreditCard
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set is_bookmark
     *
     * @param boolean $isBookmark
     * @return CreditCard
     */
    public function setIsBookmark($isBookmark)
    {
        $this->is_bookmark = $isBookmark;

        return $this;
    }

    /**
     * Get is_bookmark
     *
     * @return boolean
     */
    public function getIsBookmark()
    {
        return $this->is_bookmark;
    }

    /**
     * Set is_removed
     *
     * @param boolean $isRemoved
     * @return CreditCard
     */
    public function setIsRemoved($isRemoved)
    {
        $this->is_removed = $isRemoved;

        return $this;
    }

    /**
     * Get is_removed
     *
     * @return boolean
     */
    public function getIsRemoved()
    {
        return $this->is_removed;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return CreditCard
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set user
     *
     * @param \ApiBundle\Entity\User $user
     * @return CreditCard
     */
    public function setUser(\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add orders
     *
     * @param \ApiBundle\Entity\Order $orders
     * @return CreditCard
     */
    public function addOrder(\ApiBundle\Entity\Order $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \ApiBundle\Entity\Order $orders
     */
    public function removeOrder(\ApiBundle\Entity\Order $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set auth_code
     *
     * @param integer $authCode
     * @return CreditCard
     */
    public function setAuthCode($authCode)
    {
        $this->auth_code = $authCode;

        return $this;
    }

    /**
     * Get auth_code
     *
     * @return integer 
     */
    public function getAuthCode()
    {
        return $this->auth_code;
    }
}
