<?php

/**
 * Created by PhpStorm.
 * User: mario
 * Date: 03-11-2015
 * Time: 19:00
 */

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 * @ORM\Table(name="administrator")
 * @DoctrineAssert\UniqueEntity(fields="email", message="El email ya existe")
 */
class Admin implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     * @Assert\NotBlank(message="Este campo no puede quedar vacio")
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string $email
     * @Assert\Email(message = "El email {{ value }} no es valido.", checkMX = false)
     * @Assert\NotBlank(message="Debe ingresar un email")
     * @ORM\Column(type="string", nullable=true)
     */
    protected $email;

    /**
     * @var string $phone
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone;

    /**
     * @var string $salt
     *
     * @ORM\Column(type="string")
     */
    protected $salt;

    /**
     * @var string $password
     * @Assert\NotBlank(message="Debe ingresar una contraseña")
     * @Assert\Length(
     *      min = 6,
     *      minMessage = "La contraseña debe contener al menos {{ limit }} caracteres.",
     * )
     * @ORM\Column(type="string")
     */
    protected $password;

    private static $var = 1;

    public function getNumbers()
    {
        return self::$var++;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Implementation of UserInterface
     */
    public function getRoles()
    {
        return array('ROLE_ADMIN');
    }

    public function getUsername()
    {
        return $this->getEmail();
    }

    public function eraseCredentials()
    {

    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        if (!empty($password)) {
            $this->setSalt(md5(time()));
            $encoder = new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('sha512', true, 1);
            $password2 = $encoder->encodePassword($password, $this->getSalt());
            $this->password = $password2;
        } else {
            $this->password = $this->getPassword();
        }
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Admin
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Admin
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Admin
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }
}
