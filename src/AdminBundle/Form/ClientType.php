<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $type = $options['edit'];
        $builder
            ->add('business_name', null, array(
                'label' => 'Nombre de Empresa',
                'attr'  => array('class' => 'form-control')
            ))
            ->add('fantasy_name', null, array(
                'label' => 'Nombre de Fantasia',
                'attr'  => array('class' => 'form-control')
            ))
            ->add('rut', null, array(
                'label' => 'Rut',
                'attr'  => array('class' => 'form-control')
            ))
            ->add('email', null, array(
                'label' => 'E-mail',
                'attr'  => array('class' => 'form-control')
            ))
            ->add('address', null, array(
                'label' => 'Dirección',
                'attr'  => array('class' => 'form-control')
            ))
            ->add('city', null, array(
                'label' => 'Ciudad',
                'attr'  => array('class' => 'form-control')
            ))
            ->add('phone_number', null, array(
                'label' => 'Teléfono',
                'attr'  => array('class' => 'form-control')
            ))
            /*->add('password', 'repeated', array(
                'type'            => 'password',
                'invalid_message' => 'Los campos de contraseña deben coincidir.',
                'options'         => array('attr' => array('class' => 'password-field')),
                'required'        => $type ? false : true,
                'first_options'   => array('label' => $type ? 'Contraseña' : 'Contraseña *', 'attr' => array('class' => 'form-control')),
                'second_options'  => array('label' => $type ? 'Repite la Contraseña' : 'Repite la Contraseña *', 'attr' => array('class' => 'form-control')),
            ))*/
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Client',
            'edit'       => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'apibundle_client';
    }
}
