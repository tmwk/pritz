<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdminType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $type = $options['edit'];
        $builder
            ->add('name', null, array('label' => 'Nombre', 'attr' => array('class' => 'form-control')))
            ->add('email', null, array('label' => 'E-mail', 'attr' => array('class' => 'form-control')))
            ->add('phone', null, array('label' => 'Teléfono', 'attr' => array('class' => 'form-control')))
            ->add('password', 'repeated', array(
                'type'            => 'password',
                'invalid_message' => 'Los campos de contraseña deben coincidir.',
                'options'         => array('attr' => array('class' => 'password-field')),
                'required'        => $type ? false : true,
                'first_options'   => array('label' => 'Contraseña', 'attr' => array('class' => 'form-control')),
                'second_options'  => array('label' => 'Repite la Contraseña', 'attr' => array('class' => 'form-control')),
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\Admin',
            'edit' => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_admin';
    }
}
