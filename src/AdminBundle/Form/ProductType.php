<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                    'label' => 'Nombre',
                    'attr'  => array(
                        'class' => 'form-control'
                    ))
            )
            ->add('description', null, array(
                    'label' => 'Descripción',
                    'attr'  => array(
                        'class' => 'form-control'
                    ))
            )
            ->add('price', null, array(
                    'label' => 'Precio',
                    'attr'  => array(
                        'class' => 'form-control'
                    ))
            )
            ->add('price_hh', null, array(
                    'label' => 'Precio Happy Hour',
                    'attr'  => array(
                        'class' => 'form-control'
                    ))
            )
            ->add('category', null, array(
                    'label' => 'Categoría',
                    'attr'  => array(
                        'class' => 'form-control'
                    ))
            )
            ->add('tax_type', null, array(
                    'label' => 'Impuesto',
                    'attr'  => array(
                        'class' => 'form-control'
                    ))
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Product',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'apibundle_product';
    }
}
