<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConfigType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('app_active', null, array(
                'label' => 'Aplicación',
                'attr' => array(
                    'data-on-text'  => 'Activada',
                    'data-off-text' => 'Desactivada',
                    'class'    => ' make-switch'
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Config',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_config';
    }
}
