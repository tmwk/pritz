<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BarmanType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', null, array(
                    'label' => 'Nombre',
                    'attr'  => array(
                        'class' => 'form-control'
                    ))
            )
            ->add('lastname', null, array(
                    'label' => 'Apellidos',
                    'required' => true,
                    'attr'  => array(
                        'class' => 'form-control'
                    ))
            )
            ->add('email', null, array(
                    'label' => 'E-mail',
                    'attr'  => array(
                        'class' => 'form-control'
                    ))
            )
            ->add('rut', null, array(
                    'label' => 'Rut',
                    'attr'  => array(
                        'class' => 'form-control'
                    ))
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\User',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'apibundle_user';
    }
}
