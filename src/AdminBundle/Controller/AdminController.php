<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AdminBundle\Entity\Admin;
use AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Admin controller.
 *
 * @Route("/administrators")
 */
class AdminController extends Controller
{

    /**
     * Lists all Admin entities.
     *
     * @Route("/", name="backend_administradores")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AdminBundle:Admin')->findAll();

        return $this->render('AdminBundle:admin:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Admin entity.
     *
     * @Route("/create", name="backend_administradores_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $entity = new Admin();
        $form   = $this->createForm(new AdminType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_administradores_show', array('id' => $entity->getId())));
        }

        $var = $this->renderView('AdminBundle:admin:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));

        return new Response($var, 404);
    }


    /**
     * Displays a form to create a new Admin entity.
     *
     * @Route("/new", name="backend_administradores_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $entity = new Admin();
        $form   = $this->createForm(new AdminType(), $entity);

        return $this->render('AdminBundle:admin:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Admin entity.
     *
     * @Route("/{id}/show", name="backend_administradores_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Admin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Admin entity.');
        }

        return $this->render('AdminBundle:admin:show.html.twig', array(
            'entity' => $entity,
        ));
    }

    /**
     * Displays a form to edit an existing Admin entity.
     *
     * @Route("/{id}/edit", name="backend_administradores_edit")
     * @Method("GET")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Admin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Admin entity.');
        }

        $editForm = $this->createForm(new AdminType(), $entity, array('edit' => true));

        return $this->render('AdminBundle:admin:edit.html.twig', array(
            'entity'    => $entity,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Admin entity.
     *
     * @Route("/{id}/update", name="backend_administradores_update")
     * @Method("POST")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Admin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Admin entity.');
        }

        $editForm = $this->createForm(new AdminType(), $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('backend_administradores_edit', array('id' => $id)));
        }

        $var = $this->renderView('AdminBundle:admin:edit.html.twig', array(
            'entity'    => $entity,
            'edit_form' => $editForm->createView(),
        ));

        return new Response($var, 404);
    }

    /**
     * Deletes a Admin entity.
     *
     * @Route("/{id}/delete", name="backend_administradores_delete")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $em     = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Admin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Admin entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('backend_administradores'));
    }

    /**
     * @Route("/json", name="backend_administradores_json")
     */
    public function jsonAction()
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AdminBundle:Admin')->findAll();
        $array    = array();
        foreach ($entities as $val) {
            $options = '';
            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('backend_administradores_edit', array('id' => $val->getId())),
                'class' => 'iframe',
                'icon'  => 'icon-pencil',
                'text'  => 'Editar'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('backend_administradores_delete', array('id' => $val->getId())),
                'class'       => 'del-ajax',
                'icon'        => 'icon-trash',
                'text'        => 'Eliminar',
                'data_reload' => 'reload_backend_administradores'
            ));
            $array[] = array(
                $val->getNumbers(),
                $val->getName(),
                $val->getEmail(),
                $val->getPhone(),
                $this->renderView(':Templates:dropdown.html.twig', array(
                    'options' => $options
                ))
            );
        }

        $response = new Response();
        $response->setContent(json_encode(array(
            'aaData' => $array,
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
