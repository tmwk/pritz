<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\LocalType;
use ApiBundle\Entity\Local;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LocalController
 * @package AdminBundle\Controller
 * @Route("/local")
 */
class LocalController extends Controller
{
    /**
     * Lists all Local entities.
     *
     * @Route("/", name="admin_local")
     */
    public function indexAction()
    {
        return $this->render('AdminBundle:local:index.html.twig', array());
    }

    /**
     * Creates a new Local entity.
     *
     * @Route("/create", name="admin_local_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $entity = new Local();
        $form   = $this->createForm(new LocalType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_local_show', array('id' => $entity->getId())));
        }

        $var = $this->renderView('AdminBundle:local:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));

        return new Response($var, 404);
    }

    /**
     * Displays a form to create a new Local entity.
     *
     * @Route("/new", name="admin_local_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $entity = new Local();
        $form   = $this->createForm(new LocalType(), $entity);

        return $this->render('AdminBundle:local:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Local entity.
     *
     * @Route("/{id}/show", name="admin_local_show")
     * @Method("GET")
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ApiBundle:Local')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Local entity.');
        }

        return $this->render('AdminBundle:local:show.html.twig', array(
            'entity' => $entity,
        ));
    }

    /**
     * Displays a form to edit an existing Local entity.
     *
     * @Route("/{local}/edit", name="admin_local_edit")
     * @Method("GET")
     * @param Local $local
     * @return Response
     */
    public function editAction(Local $local)
    {
        if (!$local) {
            throw $this->createNotFoundException('Unable to find Local entity.');
        }

        $editForm = $this->createForm(new LocalType(), $local);

        return $this->render('AdminBundle:local:edit.html.twig', array(
            'entity'    => $local,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Local entity.
     *
     * @Route("/{local}/update", name="admin_local_update")
     * @Method("POST")
     * @param Request $request
     * @param Local $local
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateAction(Request $request, Local $local)
    {
        if (!$local) {
            throw $this->createNotFoundException('Unable to find Local entity.');
        }

        $editForm = $this->createForm(new LocalType(), $local);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('admin_local_edit', array('local' => $local->getId()));
        }

        $var = $this->renderView('AdminBundle:local:edit.html.twig', array(
            'entity'    => $local,
            'edit_form' => $editForm->createView(),
        ));

        return new Response($var, 404);
    }

    /**
     * Deletes a Local entity.
     *
     * @Route("/{local}/delete", name="admin_local_delete")
     * @param Local $local
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Local $local)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$local) {
            throw $this->createNotFoundException('Unable to find Local entity.');
        }

        $em->remove($local);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_local'));
    }

    /**
     * @Route("/json", name="admin_local_json")
     */
    public function jsonAction()
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ApiBundle:Local')->findAll();
        $array    = array();
        foreach ($entities as $val) {

            $options = '';

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('admin_client_local_product', array('client' => $val->getClient()->getId(), 'local' => $val->getId())),
                'class' => '',
                'icon'  => 'icon-social-dropbox',
                'text'  => 'Productos'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('admin_client_local_bartender', array('client' => $val->getClient()->getId(), 'local' => $val->getId())),
                'class' => '',
                'icon'  => 'icon-user-following',
                'text'  => 'Barman'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('admin_local_edit', array('local' => $val->getId())),
                'class' => 'iframe',
                'icon'  => 'icon-pencil',
                'text'  => 'Editar'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('admin_local_delete', array('local' => $val->getId())),
                'class'       => 'del-ajax',
                'icon'        => 'icon-trash',
                'text'        => 'Eliminar',
                'data_reload' => 'reload_admin_local'
            ));

            $array[] = array(
                $val->getNumbers(),
                $val->getName(),
                $val->getClient()->getBusinessName(),
                $val->getEnabled() ? '<span class="label label-success label-sm">Activado</span>' : '<span class="label label-danger label-sm">Desactivado</span>',
                $this->renderView(':Templates:dropdown.html.twig', array(
                    'options' => $options
                ))
            );
        }

        $response = new Response();
        $response->setContent(json_encode(array(
            'aaData' => $array,
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
