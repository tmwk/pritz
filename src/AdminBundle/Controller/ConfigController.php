<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\ConfigType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ConfigController
 * @package AdminBundle\Controller
 * @Route("/config")
 */
class ConfigController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="admin_config")
     */
    public function indexAction()
    {
        $entity = $this->getDoctrine()->getRepository('ApiBundle:Config')->findOneBy(array());
        $form   = $this->createForm(new ConfigType(), $entity);

        return $this->render('@Admin/config/index.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @Route("/update", name="admin_config_update")
     * @return Response|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ApiBundle:Config')->findOneBy(array());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

        $editForm = $this->createForm(new ConfigType(), $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirectToRoute('admin_config');
        }

        return $this->render('@Admin/config/index.html.twig', array(
            'entity' => $entity,
            'form'   => $editForm->createView()
        ));

    }
}
