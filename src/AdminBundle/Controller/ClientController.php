<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\ClientType;
use ApiBundle\Entity\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Client controller.
 *
 * @Route("/clients")
 */
class ClientController extends Controller
{

    /**
     * Lists all Client entities.
     *
     * @Route("/", name="admin_client")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ApiBundle:Client')->findAll();

        return $this->render('AdminBundle:client:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Client entity.
     *
     * @Route("/create", name="admin_client_create")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $help   = $this->get('util.helper');
        $entity = new Client();
        $form   = $this->createForm(new ClientType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $pass = $help->randomPass(8);

            $em = $this->getDoctrine()->getManager();
            $entity->setPassword($pass);
            $entity->setCreateDate(new \DateTime('now'));
            $em->persist($entity);
            $em->flush();

            $this->sendPass($entity, $pass);

            return $this->redirect($this->generateUrl('admin_client'));
        }

        $var = $this->renderView('AdminBundle:client:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));

        return new Response($var, 404);
    }


    /**
     * Displays a form to create a new Client entity.
     *
     * @Route("/new", name="admin_client_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $entity = new Client();
        $form   = $this->createForm(new ClientType(), $entity);

        return $this->render('AdminBundle:client:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Admin entity.
     *
     * @Route("/{id}/edit", name="admin_client_edit")
     * @Method("GET")
     * @param $id
     * @return Response
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ApiBundle:Client')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

        $editForm = $this->createForm(new ClientType(), $entity, array('edit' => true));

        return $this->render('AdminBundle:client:edit.html.twig', array(
            'entity'    => $entity,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Client entity.
     *
     * @Route("/{id}/update", name="admin_client_update")
     * @Method("POST")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ApiBundle:Client')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

        $editForm = $this->createForm(new ClientType(), $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_client_edit', array('id' => $id)));
        }

        $var = $this->renderView('AdminBundle:client:edit.html.twig', array(
            'entity'    => $entity,
            'edit_form' => $editForm->createView(),
        ));

        return new Response($var, 404);
    }

    /**
     * Deletes a Client entity.
     *
     * @Route("/{id}/delete", name="admin_client_delete")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $em     = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ApiBundle:Client')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Client entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_client'));
    }

    /**
     * @Route("/json", name="admin_client_json")
     */
    public function jsonAction()
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ApiBundle:Client')->findAll();
        $array    = array();
        foreach ($entities as $val) {
            $options = '';

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('admin_client_local', array('client' => $val->getId())),
                'icon'  => 'icon-home',
                'text'  => 'Locales'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('admin_client_edit', array('id' => $val->getId())),
                'class' => 'iframe',
                'icon'  => 'icon-pencil',
                'text'  => 'Editar'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'           => $this->generateUrl('admin_client_send_pass', array('id' => $val->getId())),
                'class'         => 'send-ajax',
                'icon'          => 'icon-key',
                'text'          => utf8_encode('Enviar Contrase�a'),
                'confirm_text'  => utf8_encode('�Estas seguro?'),
                'message_title' => utf8_encode('Contrase�a'),
                'message_text'  => utf8_encode('Contrase�a enviada correctamente.'),
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('admin_client_delete', array('id' => $val->getId())),
                'class'       => 'del-ajax',
                'icon'        => 'icon-trash',
                'text'        => 'Eliminar',
                'data_reload' => 'reload_admin_client'
            ));
            $array[] = array(
                $val->getNumbers(),
                $val->getBusinessName(),
                $val->getRut(),
                $val->getEmail(),
                $this->renderView(':Templates:dropdown.html.twig', array(
                    'options' => $options
                ))
            );
        }

        $response = new Response();
        $response->setContent(json_encode(array(
            'aaData' => $array,
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/{id}/send_pass", name="admin_client_send_pass")
     */
    public function sendPassAction($id)
    {
        $em     = $this->getDoctrine()->getManager();
        $helper = $this->get('util.helper');
        $pass   = $helper->randomPass(8);

        $entity = $em->getRepository('ApiBundle:Client')->find($id);
        $entity->setPassword($pass);
        $em->persist($entity);
        $em->flush();

        $this->sendPass($entity, $pass);

        return $this->redirectToRoute('admin_client');
    }

    /**
     * @param Client $client
     * @param $pass
     */
    public function sendPass(Client $client, $pass)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Pritz - Estos son sus datos de acceso')
            ->setFrom($this->getParameter('mailer_user'))
            ->setTo($client->getEmail())
            ->setBody(
                $this->renderView(
                    'AdminBundle:email:send_pass.html.twig',
                    array(
                        'entity'   => $client,
                        'password' => $pass
                    )
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }
}
