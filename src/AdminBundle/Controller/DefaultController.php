<?php

namespace AdminBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package AdminBundle\Controller
 */
class DefaultController extends FOSRestController
{
    /**
     * @return mixed
     * @Route("/", name="admin_home")
     */
    public function indexAction()
    {
        return $this->render('AdminBundle:default:index.html.twig', array(
            'context' => 'Admin'
        ));
    }
}
