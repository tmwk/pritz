<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\LocalType;
use ApiBundle\Entity\Client;
use ApiBundle\Entity\Local;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LocalController
 * @package AdminBundle\Controller
 * @Route("/client/{client}/local")
 */
class ClientLocalController extends Controller
{
    /**
     * Lists all Local entities.
     *
     * @Route("/", name="admin_client_local")
     * @param Client $client
     * @return Response
     * @internal param $client_id
     */
    public function indexAction(Client $client)
    {
        return $this->render('AdminBundle:client_local:index.html.twig', array(
            'client' => $client
        ));
    }

    /**
     * Creates a new Local entity.
     *
     * @Route("/create", name="admin_client_local_create")
     * @Method("POST")
     * @param Request $request
     * @param Client $client
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request, Client $client)
    {
        $em     = $this->getDoctrine()->getManager();
        $entity = new Local();
        $form   = $this->createForm(new LocalType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity->setCreateDate(new \DateTime('now'));
            $entity->setClient($client);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_client_local', array('client' => $client->getId())));
        }

        $var = $this->renderView('AdminBundle:client_local:new.html.twig', array(
            'entity' => $entity,
            'client' => $client,
            'form'   => $form->createView(),
        ));

        return new Response($var, 404);
    }

    /**
     * Displays a form to create a new Local entity.
     *
     * @Route("/new", name="admin_client_local_new")
     * @Method("GET")
     * @param Client $client
     * @return Response
     */
    public function newAction(Client $client)
    {
        $entity = new Local();
        $form   = $this->createForm(new LocalType(), $entity);

        return $this->render('AdminBundle:client_local:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'client' => $client
        ));
    }

    /**
     * Displays a form to edit an existing Local entity.
     *
     * @Route("/{local}/edit", name="admin_client_local_edit")
     * @Method("GET")
     * @param Client $client
     * @param Local $local
     * @return Response
     */
    public function editAction(Client $client, Local $local)
    {
        if (!$local) {
            throw $this->createNotFoundException('Unable to find Local entity.');
        }

        $editForm = $this->createForm(new LocalType(), $local);

        return $this->render('AdminBundle:client_local:edit.html.twig', array(
            'local'     => $local,
            'client'    => $client,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Local entity.
     *
     * @Route("/{local}/update", name="admin_client_local_update")
     * @Method("POST")
     * @param Client $client
     * @param Request $request
     * @param Local $local
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateAction(Client $client, Request $request, Local $local)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$local) {
            throw $this->createNotFoundException('Unable to find Local entity.');
        }

        $editForm = $this->createForm(new LocalType(), $local);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $local->setClient($client);
            $local->setLastUpdate(new \DateTime('now'));
            $em->flush();

            return $this->redirect($this->generateUrl('admin_client_local_edit', array('local' => $local->getId(), 'client' => $client->getId())));
        }

        $var = $this->renderView('AdminBundle:client_local:edit.html.twig', array(
            'local'     => $local,
            'client'    => $client,
            'edit_form' => $editForm->createView(),
        ));

        return new Response($var, 404);
    }

    /**
     * Deletes a Local entity.
     *
     * @Route("/{local}/delete", name="admin_client_local_delete")
     * @param Client $client
     * @param Local $local
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Client $client, Local $local)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$local) {
            throw $this->createNotFoundException('Unable to find Local entity.');
        }

        $em->remove($local);
        $em->flush();

        return $this->redirectToRoute('admin_client_local', array('client' => $client->getId()));
    }


    /**
     * @Route("/json", name="admin_client_local_json")
     * @param Client $client
     * @return Response
     */
    public function jsonAction(Client $client)
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ApiBundle:Local')->findBy(array('client' => $client->getId()));
        $array    = array();
        foreach ($entities as $val) {

            $options = '';

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('admin_client_local_product', array('client' => $client->getId(), 'local' => $val->getId())),
                'class' => '',
                'icon'  => 'icon-social-dropbox',
                'text'  => 'Productos'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('admin_client_local_bartender', array('client' => $val->getClient()->getId(), 'local' => $val->getId())),
                'class' => '',
                'icon'  => 'icon-user-following',
                'text'  => 'Barman'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('admin_client_local_edit', array('client' => $client->getId(), 'local' => $val->getId())),
                'class' => 'iframe',
                'icon'  => 'icon-pencil',
                'text'  => 'Editar'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('admin_client_local_delete', array('client' => $client->getId(), 'local' => $val->getId())),
                'class'       => 'del-ajax',
                'icon'        => 'icon-trash',
                'text'        => 'Eliminar',
                'data_reload' => 'reload_admin_client_local'
            ));

            $array[] = array(
                $val->getNumbers(),
                $val->getName(),
                $val->getEnabled() ? '<span class="label label-success label-sm">Activado</span>' : '<span class="label label-danger label-sm">Desactivado</span>',
                $this->renderView(':Templates:dropdown.html.twig', array(
                    'options' => $options
                ))
            );
        }

        $response = new JsonResponse();
        $response->setContent(json_encode(array('aaData' => $array)));
        return $response;
    }
}
