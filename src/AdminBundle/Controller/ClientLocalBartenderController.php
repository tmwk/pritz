<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\BarmanType;
use ApiBundle\Entity\Client;
use ApiBundle\Entity\Local;
use ApiBundle\Entity\QrScanPermission;
use ApiBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ClientLocalProductController
 * @package AdminBundle\Controller
 * @Route("/client/{client}/local/{local}/bartender")
 */
class ClientLocalBartenderController extends Controller
{
    /**
     * Lists all QrScanPermission entities.
     *
     * @Route("/", name="admin_client_local_bartender")
     * @param Client $client
     * @param Local $local
     * @return Response
     * @internal param Product $product
     * @internal param $client_id
     */
    public function indexAction(Client $client, Local $local)
    {
        return $this->render('AdminBundle:client_local_bartender:index.html.twig', array(
            'client' => $client,
            'local'  => $local,
        ));
    }

    /**
     * Creates a new QrScanPermission entity.
     *
     * @Route("/create", name="admin_client_local_bartender_create")
     * @Method("POST")
     * @param Request $request
     * @param Client $client
     * @param Local $local
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request, Client $client, Local $local)
    {
        $em     = $this->getDoctrine()->getManager();
        $help   = $this->get('util.helper');
        $barman = new User();
        $qr     = new QrScanPermission();
        $form   = $this->createForm(new BarmanType(), $barman);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $pass = $help->randomPass(8);

            $qr->setLocal($local);
            $qr->setUser($barman);
            $qr->setCreateDate(new \DateTime('now'));
            $barman->setPassword($pass);
            $barman->setRegisterDate(new \DateTime('now'));

            $em->persist($barman);
            $em->persist($qr);
            $em->flush();

            $this->sendPass($barman, $pass);

            return $this->redirectToRoute('admin_client_local_bartender', array('client' => $client->getId(), 'local' => $local->getId()));
        }

        $var = $this->renderView('AdminBundle:client_local_bartender:new.html.twig', array(
            'entity' => $barman,
            'client' => $client,
            'form'   => $form->createView(),
            'local'  => $local
        ));

        return new Response($var, 404);
    }

    /**
     * Displays a form to create a new QrScanPermission entity.
     *
     * @Route("/new", name="admin_client_local_bartender_new")
     * @Method("GET")
     * @param Client $client
     * @param Local $local
     * @return Response
     */
    public function newAction(Client $client, Local $local)
    {
        $form = $this->createForm(new BarmanType(), new User());

        return $this->render('AdminBundle:client_local_bartender:new.html.twig', array(
            'form'   => $form->createView(),
            'client' => $client,
            'local'  => $local,
        ));
    }

    /**
     * Deletes a QrScanPermission entity.
     *
     * @Route("/{barman}/delete", name="admin_client_local_bartender_delete")
     * @param Client $client
     * @param Local $local
     * @param QrScanPermission $barman
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Client $client, Local $local, QrScanPermission $barman)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$barman) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $em->remove($barman->getUser());
        $em->remove($barman);
        $em->flush();

        return $this->redirectToRoute('admin_client_local_bartender', array('client' => $client->getId(), 'local' => $local->getId()));
    }


    /**
     * @Route("/json", name="admin_client_local_bartender_json")
     * @param Client $client
     * @param Local $local
     * @return Response
     */
    public function jsonAction(Client $client, Local $local)
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ApiBundle:QrScanPermission')->findBy(array('local' => $local->getId()));

        $array = array();
        foreach ($entities as $val) {

            $options = '';

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'           => $this->generateUrl('admin_client_local_bartender_send_pass', array('client' => $client->getId(), 'local' => $local->getId(), 'barman' => $val->getId())),
                'class'         => 'send-ajax',
                'icon'          => 'icon-key',
                'text'          => 'Enviar Contraseña',
                'confirm_text'  => '¿Estas seguro?',
                'message_title' => 'Contraseña',
                'message_text'  => 'Contraseña enviada correctamente.',
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('admin_client_local_bartender_delete', array('client' => $client->getId(), 'local' => $local->getId(), 'barman' => $val->getId())),
                'class'       => 'del-ajax',
                'icon'        => 'icon-trash',
                'text'        => 'Quitar Permisos',
                'data_reload' => 'reload_admin_client_local_bartender'
            ));

            $array[] = array(
                $val->getNumbers(),
                $val->getUser()->getFullName(),
                $val->getUser()->getRut(),
                $val->getUser()->getEmail(),
                $this->renderView(':Templates:dropdown.html.twig', array(
                    'options' => $options
                ))
            );
        }

        $response = new JsonResponse();
        $response->setContent(json_encode(array('aaData' => $array)));
        return $response;
    }

    /**
     * @Route("/{barman}/send_pass", name="admin_client_local_bartender_send_pass")
     * @param Client $client
     * @param Local $local
     * @param QrScanPermission $barman
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sendPassAction(Client $client, Local $local, QrScanPermission $barman)
    {
        $em     = $this->getDoctrine()->getManager();
        $helper = $this->get('util.helper');
        $pass   = $helper->randomPass(8);

        $entity = $barman->getUser();
        $entity->setPassword($pass);
        $em->persist($entity);
        $em->flush();

        $this->sendPass($entity, $pass);

        return $this->redirectToRoute('admin_client');
    }

    /**
     * @param User $user
     * @param $pass
     */
    public function sendPass(User $user, $pass)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Pritz - Estos son sus datos de acceso')
            ->setFrom($this->getParameter('mailer_user'))
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'AdminBundle:email:send_pass.html.twig',
                    array(
                        'entity'   => $user,
                        'password' => $pass
                    )
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }
}
