<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\LocalType;
use AdminBundle\Form\ProductType;
use ApiBundle\Entity\Client;
use ApiBundle\Entity\Local;
use ApiBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ClientLocalProductController
 * @package AdminBundle\Controller
 * @Route("/client/{client}/local/{local}/product")
 */
class ClientLocalProductController extends Controller
{
    /**
     * Lists all Local entities.
     *
     * @Route("/", name="admin_client_local_product")
     * @param Client $client
     * @param Local $local
     * @return Response
     * @internal param Product $product
     * @internal param $client_id
     */
    public function indexAction(Client $client, Local $local)
    {
        return $this->render('AdminBundle:client_local_product:index.html.twig', array(
            'client' => $client,
            'local'  => $local,
        ));
    }

    /**
     * Creates a new Local entity.
     *
     * @Route("/create", name="admin_client_local_product_create")
     * @Method("POST")
     * @param Request $request
     * @param Client $client
     * @param Local $local
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request, Client $client, Local $local)
    {
        $em     = $this->getDoctrine()->getManager();
        $entity = new Product();
        $form   = $this->createForm(new ProductType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity->setLocal($local);
            $entity->setCreateDate(new \DateTime('now'));
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('admin_client_local_product', array('client' => $client->getId(), 'local' => $local->getId()));
        }

        $var = $this->renderView('AdminBundle:client_local_product:new.html.twig', array(
            'entity' => $entity,
            'client' => $client,
            'form'   => $form->createView(),
            'local'  => $local
        ));

        return new Response($var, 404);
    }

    /**
     * Displays a form to create a new Local entity.
     *
     * @Route("/new", name="admin_client_local_product_new")
     * @Method("GET")
     * @param Client $client
     * @param Local $local
     * @return Response
     */
    public function newAction(Client $client, Local $local)
    {
        $form = $this->createForm(new ProductType(), new Product());

        return $this->render('AdminBundle:client_local_product:new.html.twig', array(
            'form'   => $form->createView(),
            'client' => $client,
            'local'  => $local,
        ));
    }

    /**
     * Displays a form to edit an existing Local entity.
     *
     * @Route("/{product}/edit", name="admin_client_local_product_edit")
     * @Method("GET")
     * @param Client $client
     * @param Local $local
     * @param Product $product
     * @return Response
     */
    public function editAction(Client $client, Local $local, Product $product)
    {
        if (!$product) {
            throw $this->createNotFoundException('Unable to find Local entity.');
        }

        $editForm = $this->createForm(new ProductType(), $product);

        return $this->render('AdminBundle:client_local_product:edit.html.twig', array(
            'local'     => $local,
            'client'    => $client,
            'product'   => $product,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Local entity.
     *
     * @Route("/{product}/update", name="admin_client_local_product_update")
     * @Method("POST")
     * @param Client $client
     * @param Request $request
     * @param Local $local
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updateAction(Client $client, Request $request, Local $local, Product $product)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$product) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $editForm = $this->createForm(new ProductType(), $product);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $local->setLastUpdate(new \DateTime('now'));
            $em->flush();

            return $this->redirectToRoute('admin_client_local_product_edit', array('local' => $local->getId(), 'client' => $client->getId(), 'product' => $product->getId()));
        }

        $var = $this->renderView('AdminBundle:client_local:edit.html.twig', array(
            'local'     => $local,
            'client'    => $client,
            'product'   => $product,
            'edit_form' => $editForm->createView(),
        ));

        return new Response($var, 404);
    }

    /**
     * Deletes a Local entity.
     *
     * @Route("/{product}/delete", name="admin_client_local_product_delete")
     * @param Client $client
     * @param Local $local
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Client $client, Local $local, Product $product)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$product) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('admin_client_local_product', array('client' => $client->getId(), 'local' => $local->getId()));
    }


    /**
     * @Route("/json", name="admin_client_local_product_json")
     * @param Client $client
     * @param Local $local
     * @return Response
     */
    public function jsonAction(Client $client, Local $local)
    {
        $em       = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ApiBundle:Product')->findBy(array('local' => $local->getId()));

        $array = array();
        foreach ($entities as $val) {

            $options = '';

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'   => $this->generateUrl('admin_client_local_product_edit', array('client' => $client->getId(), 'local' => $local->getId(), 'product' => $val->getId())),
                'class' => 'iframe',
                'icon'  => 'icon-pencil',
                'text'  => 'Editar'
            ));

            $options .= $this->renderView(':Templates:link_json.html.twig', array(
                'url'         => $this->generateUrl('admin_client_local_product_delete', array('client' => $client->getId(), 'local' => $local->getId(), 'product' => $val->getId())),
                'class'       => 'del-ajax',
                'icon'        => 'icon-trash',
                'text'        => 'Eliminar',
                'data_reload' => 'reload_admin_client_local_product'
            ));

            $array[] = array(
                $val->getNumbers(),
                $val->getName(),
                $val->getPrice(),
                $this->renderView(':Templates:dropdown.html.twig', array(
                    'options' => $options
                ))
            );
        }

        $response = new JsonResponse();
        $response->setContent(json_encode(array('aaData' => $array)));
        return $response;
    }
}
