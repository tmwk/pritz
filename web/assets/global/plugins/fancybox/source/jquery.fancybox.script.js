$(document).ready(function () {
    $(".iframe").fancybox({autoScale: true, autoDimensions: false, transitionIn: "none", transitionOut: "none", type: "ajax", padding: "0", modal: true, autoSize : false, height :"auto"});
    $(".iframe_iframe").fancybox({autoScale: true, transitionIn: "none", transitionOut: "none", type: "iframe", padding: "0"});
    $(".iframe_editor").fancybox({
        autoScale: true,
        transitionIn: "none",
        transitionOut: "none",
        type: "ajax",
        padding: "0",
        //modal: true,
        afterShow:function() {
            CKEDITOR.replaceAll( function(textarea, config) {
                if (textarea.className=="ckeditor") return false;
                if (textarea.id=="summary") return false;
//configuration????
                return true;
            });
        }
    });



    $(".iframe_image").fancybox({autoScale: true, transitionIn: "none", transitionOut: "none" });
    $(".close-fancybox").live("click", function () {
        $.fancybox.close();
    });
});