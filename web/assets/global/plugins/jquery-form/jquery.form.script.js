$(document).ready(function () {
    $(".iframe_form").live("submit", function (e) {
        $(".fancybox-inner").append('<div class="showLoading"></div>');
        var target = $(this);
        e.preventDefault();
        $(this).ajaxSubmit({onsubmit: function (event) {
        }, success: function (data, textStatus, jqXHR) {
            if (target.attr("data-reload")) {
                if (target.attr("data-type") == "new") {

                    if(target.attr("data-orden") == 'true'){
                        $.get('/print/'+data+'/new', function(orden){
                            $('.dropdown-menu.extended.notification').prepend(orden);
                            var total_print = $('.total_ordenes_print');
                            var total_print_int = $('.total_ordenes_print_int');

                            var actual_print = parseInt(total_print.text());
                            total_print.text(actual_print+1);
                            total_print_int.text(actual_print+1);
                        })
                    }

                    $("." + target.attr("data-reload")).dataTable().fnReloadAjax();



                }else if (target.attr("data-type") == "normal") {


                } else {

                    if (target.attr("data-type") == "edit") {
                        $("." + target.attr("data-reload")).dataTable().fnReloadAjax();
                    } else {
                        if (target.attr("data-type") == "update_image") {
                            $("." + target.attr("data-reload")).attr("src", data);
                            if (target.attr("data-logo")) {
                                $("." + target.attr("data-logo")).attr("src", data);
                            }
                        }
                    }

                }
            } else {
                $(".sample_1").each(function () {
                    $(this).dataTable().fnReloadAjax();
                });
                $(".sample_2").each(function () {
                    $(this).dataTable().fnReloadAjax();
                });
            }
            $.fancybox.close();
            if (target.attr("data-type") == "new") {
                toastr['success']("Registro agregado correctamente.", "Registros");
            } else {
                if (target.attr("data-type") == "edit") {
                    toastr['success']("Registro actualizado correctamente.", "Registros");
                } else {
                    if (target.attr("data-type") == "update_image") {
                    }
                }
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            if (target.attr("data-type") == "new") {
                toastr['error']("Error al agregar el registro.", "Registros");
            }else if (target.attr("data-type") == "edit") {
                toastr['error']("Error al actualizar el registro.", "Registros");
            }else if (target.attr("data-type") == "update_image") {
                toastr['error']("Error al actualizar el registro.", "Registros");
            }else if (target.attr("data-type") == "normal") {

            }
            $(".fancybox-inner").html(jqXHR.responseText);
            $(".showLoading").remove();
        }});
        e.stopImmediatePropagation();
    });
});