(function ($) {
    jQuery.fn.jConfirmAction = function (options) {
        var theOptions = jQuery.extend({
            question: "¿Desea eliminar?",
            yesAnswer: "Yes",
            cancelAnswer: "Cancel",
            callback: false
        }, options);
        this.live("click", function (e) {
            e.preventDefault();
            thisHref = $(this).attr("href");
            if($(this).data('confirm-text')){
                theOptions.question = $(this).data('confirm-text');
            }
            var target = $(this), question = target.data("question.jConfirmAction");
            e.preventDefault();
            if (!question) {
                question = $('<div class="question">' + theOptions.question + '<br/> <span class="yes">' + theOptions.yesAnswer + '</span><span class="cancel">' + theOptions.cancelAnswer + "</span></div>");
                target.closest('ul').after(question).data("question.jConfirmAction", question);
            }
            question.animate({opacity: 1}, 300);
            $(".yes", question).unbind("click.jConfirmAction").bind("click.jConfirmAction", function () {
                if ($.isFunction(theOptions.callback)) {
                    theOptions.callback(target);
                    question.fadeOut(300, function () {
                        question.remove();
                        target.removeData("question.jConfirmAction");
                    });
                } else {
                    window.location = thisHref;
                }
            });
            $(".cancel", question).unbind("click.jConfirmAction").bind("click.jConfirmAction", function () {
                question.fadeOut(300, function () {
                    question.remove();
                    target.removeData("question.jConfirmAction");
                });
            });
        });
    };
})(jQuery);
(function ($) {
    $("a.simple").jConfirmAction();
    $("a.callback").jConfirmAction({
        callback: function () {
            alert("the callback is called");
        }
    });
    $("a.anothercallback").jConfirmAction({
        callback: function () {
            alert("the anothercallback is called");
        }
    });
    $("a.callbackid").jConfirmAction({
        callback: function (target) {
            alert(target.attr("id"));
        }
    });
})(jQuery);