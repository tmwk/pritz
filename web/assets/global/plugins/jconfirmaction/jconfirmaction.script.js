$(document).ready(function () {
    $(".del-ajax").jConfirmAction({
        yesAnswer: "Si", cancelAnswer: "No", callback: function (target) {
            $.ajax({
                type: "POST", url: target.attr("href"), data: target.serialize(), success: function (data) {
                    if (target.data("reload")) {
                        $("." + target.data("reload")).dataTable().fnReloadAjax();
                    } else {
                        $(".sample_1").each(function () {
                            $(this).dataTable().fnReloadAjax();
                        });
                    }
                    if (target.data("class") == "profile_image_delete") {
                        $.get(target.attr("href"), function (data) {
                            $(".profile_image").attr("src", "/public/img/no_images.jpg");
                        });
                    }
                    toastr['success']("Registro eliminado correctamente", "Registro");
                }, fail: function () {
                    toastr['error']("Error al eliminar el registro", "Registro");
                }
            });
        }
    });


    $(".del-redir").jConfirmAction({
        yesAnswer: "Si", cancelAnswer: "No", callback: function (target) {
            window.location.href = target.attr('href');
        }
    });

    $(".send-ajax").jConfirmAction({
        yesAnswer: "Si", cancelAnswer: "No", callback: function (target) {
            $.get(target.attr('href'))
                .success(function (data) {
                    if (target.data('message-text') && target.data('message-title')) {
                        toastr['success'](target.data('message-text'), target.data('message-title'));
                    }
                });
        }
    });

});