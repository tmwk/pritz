/**
 * Created by Mario on 01-12-2015.
 */

$(document).ready(function () {

    document.oncontextmenu = function(){return false};



    $('.reloj').countDown({
        css_class: 'countdown-alt-1',
        with_labels: false
    });

    window.fbAsyncInit = function () {
        FB.init({
            appId: '500872593426826',
            xfbml: true,
            version: 'v2.5'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $('.share_fb').click(function (e) {
        e.preventDefault();
        var target = $(this);
        FB.ui({
            method: 'share',
            href: target.data("share")
        }, function (response) {
        })
    });


});