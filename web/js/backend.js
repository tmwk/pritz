$(document).ready(function() {


    $('#ajax_tables').DataTable( {
        ajax: $(this).data("url"),
        sDom: "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        bProcessing: true,
        oLanguage: {
            sName: "asd",
            sLoadingRecords: 'Cargando... <img src="/assets/global/img/loading.gif" width="16" alt="3D snake">',
            sProcessing: '<img src="/assets/global/img/loading.gif" width="16" alt="3D snake">',
            sLengthMenu: "Mostrar _MENU_ registros por pagina",
            sZeroRecords: "Lo sentimos, no hay registros",
            sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
            sSearch: "",
            sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
            sInfoFiltered: "(filtrado de _MAX_ registros en total)",
            sEmptyTable: "No hay información para mostrar",
            sSearchPlaceholder : "Buscar"
        }
    } );


    $("#ajax_table").html(function () {
        var target = $(this);
        target.dataTable({
            sDom: "<'row-fluid'<''l><''f>r>t<'row-fluid'<''i><''p>>",
            sAjaxSource: target.data("url"),
            bProcessing: true,
            bLengthChange: true,
            pageLength: 50,
            oLanguage: {
                sName: "asd",
                sLoadingRecords: 'Cargando Contenido...',
                sProcessing: 'Cargando...',
                sLengthMenu: "Mostrar _MENU_ registros por pagina",
                sZeroRecords: "Lo sentimos, no hay registros",
                sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
                sSearch: "",
                sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros en total)",
                sEmptyTable: "No hay información para mostrar",
                sSearchPlaceholder : "Buscar"
            },
            aoColumnDefs: [{bSortable: false, aTargets: [0]}]
        });
    });

    $("#ajax_table_ss").html(function () {
        var target = $(this);
        target.dataTable({
            sDom: "<'row-fluid'<''l><''f>r>t<'row-fluid'<''i><''p>>",
            sAjaxSource: target.data("url"),
            bProcessing: true,
            bLengthChange: true,
            serverSide: true,
            pageLength: 50,
            oLanguage: {
                sName: "asd",
                sLoadingRecords: 'Cargando Contenido...',
                sProcessing: 'Cargando...',
                sLengthMenu: "Mostrar _MENU_ registros por pagina",
                sZeroRecords: "Lo sentimos, no hay registros",
                sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
                sSearch: "",
                sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros en total)",
                sEmptyTable: "No hay información para mostrar",
                sSearchPlaceholder : "Buscar"
            },
            aoColumnDefs: [{bSortable: false, aTargets: [0]}]
        });
    });

    $('body').delegate('#category', 'change', function (e) {
        $("#ajax_table_ss_category").dataTable().fnReloadAjax();
    });

    $("#ajax_table_ss_category").html(function () {
        var target = $(this);
        target.dataTable({
            sDom: "<'row-fluid'<''l><''f>r>t<'row-fluid'<''i><''p>>",
            sAjaxSource: target.data("url")+'?category=',
            bProcessing: true,
            bLengthChange: true,
            serverSide: true,
            pageLength: 50,
            fnServerParams: function ( aoData ) {
                aoData.push( { "name": "category", "value": $('#category option:selected').val() } );
            },
            oLanguage: {
                sName: "asd",
                sLoadingRecords: 'Cargando Contenido...',
                sProcessing: 'Cargando...',
                sLengthMenu: "Mostrar _MENU_ registros por pagina",
                sZeroRecords: "Lo sentimos, no hay registros",
                sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
                sSearch: "",
                sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros en total)",
                sEmptyTable: "No hay información para mostrar",
                sSearchPlaceholder : "Buscar"
            },
            aoColumnDefs: [{bSortable: false, aTargets: target.data('exclude-sort')}]
        });
    });

    $("#ajax_order_table").html(function () {
        var target = $(this);
        document.test = target.dataTable({
            sDom: "<'row-fluid'<''l><''f>r>t<'row-fluid'<''i><''p>>",
            sAjaxSource: target.data("url"),
            bProcessing: true,
            bLengthChange: false,
            pageLength: 50,
            oLanguage: {
                sName: "asd",
                sLoadingRecords: 'Cargando Contenido...',
                sProcessing: 'Cargando...',
                sLengthMenu: "Mostrar _MENU_ registros por pagina",
                sZeroRecords: "Lo sentimos, no hay registros",
                sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
                sSearch: "",
                sInfoEmpty: "Mostrando 0 a 0 de 0 registros",
                sInfoFiltered: "(filtrado de _MAX_ registros en total)",
                sEmptyTable: "No hay información para mostrar",
                sSearchPlaceholder : "Buscar"
            },
            fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                nRow.setAttribute('id', aData.RowOrder);
                $(nRow).find('td:eq(0)').click(function (e) {
                    e.stopImmediatePropagation();

                    var toPosition = prompt("Ingrese la posición deseada"),
                        fromPosition = $(this).text(),
                        id = $(this).closest('tr').attr('id'),
                        direction;

                    if(toPosition == fromPosition ||  toPosition == '' ||  toPosition == null) return false;

                    if(fromPosition > toPosition) direction = 'back';

                    if(fromPosition < toPosition) direction = 'forward';

                    $.post(target.data("url-order"), { id: id, fromPosition: $(this).text(), toPosition: toPosition, direction: direction }, function(){
                        toastr['success']("Posicion actualizada correctamente", "Cambio de Posicion");
                        target.dataTable().fnReloadAjax();
                    } );

                });

                return nRow;
            },
            aoColumnDefs: [{bSortable: false, aTargets: [0]}]
        }).rowReordering({
            sURL:target.data("url-order"),
            sRequestType: "POST",
            feedBack: function (text) {
                toastr['success']("Posicion actualizada correctamente", "Cambio de Posicion");
                target.dataTable().fnReloadAjax();
            },
            fnAlert: function(text){
                alert("Order canot be changed on the server-side.\n" + text);
            }
        });

    });


    $('body').delegate('.add-response', 'click', function (e) {
        e.preventDefault();
        $(this).closest('.response').after($(this).closest('.response').clone());

        $(this).closest('.response').after().find('input[type=checkbox]').bootstrapSwitch();

        ComponentsEditors.init();
    });


    $('body').delegate('.check-correct', 'click', function(e){
        e.preventDefault();
        var target = $(this);
        $.get($(this).data('url'), function (data) {
            var allCheck = $(".check-correct");
            allCheck.removeClass("active");
            allCheck.removeClass("btn-success");
            allCheck.addClass("btn-default");
            target.addClass("active");
            target.addClass("btn-success");
            target.removeClass("btn-default");
            //target.closest('table').dataTable().fnReloadAjax();
        });
    });

    $('body').delegate('.ver_mas', 'click', function(e){
        e.preventDefault();
        $(".ver_mas_content").slideToggle('slow', function () {
        });
    });



    $('body').delegate('.question-add', 'click', function(e){
        e.preventDefault();
        $.get($(this).attr('href'), function (data) {
            $('.backend_cuestionario_questions').dataTable().fnReloadAjax();
            $('.reload_backend_cuestionario_questions_add').dataTable().fnReloadAjax();
        })
    });





    $.fn.setCursorPosition = function(pos) {
        this.each(function(index, elem) {
            if (elem.setSelectionRange) {
                elem.setSelectionRange(pos, pos);
            } else if (elem.createTextRange) {
                var range = elem.createTextRange();
                range.collapse(true);
                range.moveEnd('character', pos);
                range.moveStart('character', pos);
                range.select();
            }
        });
        return this;
    };


} );


(function ($, undefined) {
    $.fn.getCursorPosition = function () {
        var el = $(this).get(0);
        var pos = 0;
        if ('selectionStart' in el) {
            pos = el.selectionStart;
        } else if ('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    }
})(jQuery);